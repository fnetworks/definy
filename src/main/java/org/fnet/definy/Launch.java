package org.fnet.definy;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * The main entry point class.
 */
public class Launch {

	public static void main(String[] args) {
		Properties buildProperties = new Properties();
		try (InputStream buildProps = Launch.class.getResourceAsStream("/build.properties")) {
			buildProperties.load(buildProps);
		} catch (IOException e) {
			System.err.println("Could not read build properties");
			e.printStackTrace();
		}
		Application.instance = new Application(args, buildProperties);
	}

}
