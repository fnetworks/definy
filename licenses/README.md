# Open source licenses

* [GSON](gson.txt)
* [GUAVA](guava.txt)
* [Java Image Scaling](java-image-scaling.txt)
* [Log4J2](log4j2.txt)
* [RSyntaxTextArea](rsyntaxtextarea.txt)
* [SLF4J](slf4j.txt)
* [SVG Salamander](svg-salamander.txt)
* [WebLaF](weblaf.txt)
* [XStream](xstream.txt)