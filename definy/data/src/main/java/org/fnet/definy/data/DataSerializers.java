package org.fnet.definy.data;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.ServiceLoader;

public class DataSerializers {

	private static final Map<String, DataSerializer> SERIALIZERS = new HashMap<>();

	static {
		for (DataSerializer serializer : ServiceLoader.load(DataSerializer.class))
			SERIALIZERS.put(serializer.id(), serializer);
	}

	/**
	 * Register a new DataSerializer.
	 *
	 * @param key        The key under which the serializer will be found. Normally the id.
	 * @param serializer The serializer to register.
	 */
	public static void register(String key, DataSerializer serializer) {
		Objects.requireNonNull(key, "key");
		Objects.requireNonNull(serializer, "serializer");
		if (SERIALIZERS.containsKey(key))
			throw new RuntimeException("DataSerializer for " + key + " already registered");
		SERIALIZERS.put(key, serializer);
	}

	/**
	 * Register a new DataSerializer.
	 * The serializers id will be used as key.
	 *
	 * @param serializer The serializer to register.
	 */
	public static void register(DataSerializer serializer) {
		Objects.requireNonNull(serializer, "serializer");
		String key = serializer.id();
		if (SERIALIZERS.containsKey(key))
			throw new RuntimeException("DataSerializer for " + key + " already registered");
		SERIALIZERS.put(key, serializer);
	}

	/**
	 * Gets a serializer by the key.
	 *
	 * @param key The key to look for.
	 * @return The serializer that corresponds to the key, or null if there is none.
	 */
	public static DataSerializer get(String key) {
		return SERIALIZERS.get(key);
	}

	/**
	 * Gets a serializer that supports a given file.
	 *
	 * @param file The file that the serializer should support.
	 * @return A serializer that can handle the file, or null if there is none.
	 * @see DataSerializer#canHandle(File)
	 */
	public static DataSerializer get(File file) {
		return SERIALIZERS.values().stream()
				.filter(e -> e.canHandle(file))
				.findAny()
				.orElse(null);
	}

	/**
	 * Serializes a {@link DataObject} to a file.
	 * Shorthand for {@code get(file).serialize(o, file)}.
	 *
	 * @param o    The DataObject to serialize
	 * @param file The file to write raw data to.
	 * @throws IOException If an I/O error occurs
	 */
	public static void serialize(DataObject o, File file) throws IOException {
		get(file).serialize(o, file);
	}

	/**
	 * Deserializes a {@link DataObject} from a file.
	 * Shorthand for {@code get(file).deserialize(file)}.
	 *
	 * @param file The file to read raw data from.
	 * @return The deserialized DataObject.
	 * @throws IOException If an I/O error occurs
	 */
	public static DataObject deserialize(File file) throws IOException {
		return get(file).deserialize(file);
	}

	/**
	 * Deserializes a {@link DataObject} from a file.
	 * Shorthand for {@code get(file).deserialize(file)}.
	 *
	 * @param id The id of the serializer to use.
	 * @param in The InputStream to read raw data from.
	 * @return The deserialized DataObject.
	 * @throws IOException If an I/O error occurs
	 */
	public static DataObject deserialize(String id, InputStream in) throws IOException {
		return get(id).deserialize(in);
	}
}
