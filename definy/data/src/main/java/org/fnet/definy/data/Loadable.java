package org.fnet.definy.data;

/**
 * Interface for classes that can be loaded from a {@link DataObject}.
 */
public interface Loadable {

	void load(DataObject data);

}
