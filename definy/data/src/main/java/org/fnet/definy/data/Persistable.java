package org.fnet.definy.data;

/**
 * Interface for classes that can be saved to and loaded from a {@link DataObject}.
 */
public interface Persistable extends Loadable {

	void save(DataObject data);

}
