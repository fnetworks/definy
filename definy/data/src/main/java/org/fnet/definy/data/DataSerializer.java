package org.fnet.definy.data;

import java.io.*;

/**
 * Responsible for converting {@link DataObject}s from and to raw data.
 *
 * @see DataSerializers
 */
public interface DataSerializer {

	/**
	 * @return The descriptive id of this serializer (e.g. {@code json}).
	 */
	String id();

	/**
	 * Checks whether this serializer can handle the given file.
	 * This checks by file extension, so the file doesn't need to exist beforehand.
	 *
	 * @param file The file to check.
	 * @return Whether this serializer can handle this file.
	 */
	boolean canHandle(File file);

	/**
	 * Serializes the data object into raw data.
	 *
	 * @param object The object to serialize.
	 * @param out    The OutputStream to write the raw data into.
	 * @throws IOException If an I/O error occurs
	 */
	void serialize(DataObject object, OutputStream out) throws IOException;

	/**
	 * Serializes the data object into raw data.
	 *
	 * @param object The object to serialize.
	 * @param file   The file to write the raw data into.
	 * @throws IOException If an I/O error occurs
	 */
	default void serialize(DataObject object, File file) throws IOException {
		try (OutputStream out = new FileOutputStream(file)) {
			serialize(object, out);
		}
	}

	/**
	 * Deserializes a data object from raw data.
	 *
	 * @param in The InputStream to read the raw data from.
	 * @return The deserialized DataObject
	 */
	DataObject deserialize(InputStream in) throws IOException;

	/**
	 * Deserializes a data object from raw data.
	 *
	 * @param file The file to read the raw data from.
	 * @return The deserialized DataObject
	 */
	default DataObject deserialize(File file) throws IOException {
		try (InputStream in = new FileInputStream(file)) {
			return deserialize(in);
		}
	}

}
