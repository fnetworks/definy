package org.fnet.definy.data.impl.json;

import com.google.auto.service.AutoService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.fnet.definy.data.DataObject;
import org.fnet.definy.data.DataSerializer;

import java.io.*;
import java.lang.reflect.Type;
import java.util.Map;

@AutoService(DataSerializer.class)
public class JsonDataSerializer implements DataSerializer {

	private Gson gson = new Gson();

	@Override
	public String id() {
		return "json";
	}

	@Override
	public boolean canHandle(File file) {
		return file.getName().endsWith(".json");
	}

	@Override
	public void serialize(DataObject object, OutputStream out) throws IOException {
		try (OutputStreamWriter writer = new OutputStreamWriter(out)) {
			gson.toJson(object.asMap(), writer);
		}
	}

	@Override
	public DataObject deserialize(InputStream in) {
		Type typeOfMap = new TypeToken<Map<String, Object>>() {
		}.getType();
		return new DataObject(gson.fromJson(new InputStreamReader(in), typeOfMap));
	}

}
