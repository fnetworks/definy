package org.fnet.definy.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DataArray {

	private List<Object> list;

	public DataArray(List<Object> list) {
		Objects.requireNonNull(list, "list");
		this.list = list;
	}

	public DataArray() {
		this.list = new ArrayList<>();
	}

	public List<Object> asList() {
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Integer> asIntList() {
		return (List<Integer>) (List) list;
	}

	@SuppressWarnings("unchecked")
	public List<Float> asFloatList() {
		return (List<Float>) (List) list;
	}

	@SuppressWarnings("unchecked")
	public List<Double> asDoubleList() {
		return (List<Double>) (List) list;
	}

	@SuppressWarnings("unchecked")
	public List<Boolean> asBoolList() {
		return (List<Boolean>) (List) list;
	}

	@SuppressWarnings("unchecked")
	public List<String> asStringList() {
		return (List<String>) (List) list;
	}

	@SuppressWarnings("unchecked")
	public List<DataObject> asObjectList() {
		List<DataObject> ret = new ArrayList<>(list.size());
		for (Object o : list)
			ret.add(new DataObject((Map<String, Object>) o));
		return ret;
	}

	@SuppressWarnings("unchecked")
	public List<DataArray> asArrayList() {
		List<DataArray> ret = new ArrayList<>(list.size());
		for (Object o : list)
			ret.add(new DataArray((List<Object>) o));
		return ret;
	}

	public int length() {
		return list.size();
	}

	public void addInt(int value) {
		list.add(value);
	}

	public void addFloat(float value) {
		list.add(value);
	}

	public void addDouble(double value) {
		list.add(value);
	}

	public void addBoolean(boolean value) {
		list.add(value);
	}

	public void addString(String value) {
		list.add(value);
	}

	public void addObject(DataObject value) {
		list.add(value.asMap());
	}

	public void addArray(DataArray value) {
		list.add(value.asList());
	}

}
