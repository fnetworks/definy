package org.fnet.definy.data;

import org.jetbrains.annotations.NonNls;

import java.util.*;

/**
 * A data object. Wrapper around a Map.
 * Helpful when serializing data to JSON or other formats.
 *
 * @see DataSerializer
 */
public class DataObject {

	private Map<String, Object> map;

	public DataObject(Map<String, Object> map) {
		Objects.requireNonNull(map, "map");
		this.map = map;
	}

	public DataObject() {
		this.map = new HashMap<>();
	}

	public Map<String, Object> asMap() {
		return map;
	}

	public boolean has(@NonNls String key) {
		return map.containsKey(key);
	}

	public String getString(@NonNls String key) {
		return (String) map.get(key);
	}

	@SuppressWarnings("unchecked")
	public String[] getStringArray(@NonNls String key) {
		List<String> list = (List<String>) map.get(key);
		if (list == null)
			return null;
		return list.toArray(new String[0]);
	}

	public int getInt(@NonNls String key) {
		return (int) map.get(key);
	}

	@SuppressWarnings("unchecked")
	public int[] getIntArray(@NonNls String key) {
		List<Integer> list = (List<Integer>) map.get(key);
		if (list == null)
			return null;
		int[] array = new int[list.size()];
		for (int i = 0; i < list.size(); i++)
			array[i] = list.get(i);
		return array;
	}

	public float getFloat(@NonNls String key) {
		return (float) map.get(key);
	}

	@SuppressWarnings("unchecked")
	public float[] getFloatArray(@NonNls String key) {
		List<Float> list = (List<Float>) map.get(key);
		if (list == null)
			return null;
		float[] array = new float[list.size()];
		for (int i = 0; i < list.size(); i++)
			array[i] = list.get(i);
		return array;
	}

	public double getDouble(@NonNls String key) {
		return (double) map.get(key);
	}

	@SuppressWarnings("unchecked")
	public double[] getDoubleArray(@NonNls String key) {
		List<Double> list = (List<Double>) map.get(key);
		if (list == null)
			return null;
		double[] array = new double[list.size()];
		for (int i = 0; i < list.size(); i++)
			array[i] = list.get(i);
		return array;
	}

	public boolean getBoolean(@NonNls String key) {
		return (boolean) map.get(key);
	}

	@SuppressWarnings("unchecked")
	public boolean[] getBooleanArray(@NonNls String key) {
		List<Boolean> list = (List<Boolean>) map.get(key);
		if (list == null)
			return null;
		boolean[] array = new boolean[list.size()];
		for (int i = 0; i < list.size(); i++)
			array[i] = list.get(i);
		return array;
	}

	@SuppressWarnings("unchecked")
	public DataObject getObject(@NonNls String key) {
		Map<String, Object> entry = (Map<String, Object>) map.get(key);
		if (entry == null)
			throw new NullPointerException();
		return new DataObject(entry);
	}

	@SuppressWarnings("unchecked")
	public DataObject[] getObjectArray(@NonNls String key) {
		return ((List<Map<String, Object>>) map.get(key)).stream().map(DataObject::new).toArray(DataObject[]::new);
	}

	public String getString(@NonNls String key, String def) {
		return (String) map.getOrDefault(key, def);
	}

	@SuppressWarnings("unchecked")
	public String[] getStringArray(@NonNls String key, String[] def) {
		List<String> list = (List<String>) map.get(key);
		if (list == null)
			return def;
		return list.toArray(new String[0]);
	}

	public int getInt(@NonNls String key, int def) {
		return (int) map.getOrDefault(key, def);
	}

	@SuppressWarnings("unchecked")
	public int[] getIntArray(@NonNls String key, int[] def) {
		List<Integer> list = (List<Integer>) map.get(key);
		if (list == null)
			return def;
		int[] array = new int[list.size()];
		for (int i = 0; i < list.size(); i++)
			array[i] = list.get(i);
		return array;
	}

	public float getFloat(@NonNls String key, float def) {
		return (float) map.getOrDefault(key, def);
	}

	@SuppressWarnings("unchecked")
	public float[] getFloatArray(@NonNls String key, float[] def) {
		List<Float> list = (List<Float>) map.get(key);
		if (list == null)
			return def;
		float[] array = new float[list.size()];
		for (int i = 0; i < list.size(); i++)
			array[i] = list.get(i);
		return array;
	}

	public double getDouble(@NonNls String key, double def) {
		return (double) map.getOrDefault(key, def);
	}

	@SuppressWarnings("unchecked")
	public double[] getDoubleArray(@NonNls String key, double[] def) {
		List<Double> list = (List<Double>) map.get(key);
		if (list == null)
			return def;
		double[] array = new double[list.size()];
		for (int i = 0; i < list.size(); i++)
			array[i] = list.get(i);
		return array;
	}

	public boolean getBoolean(@NonNls String key, boolean def) {
		return (boolean) map.getOrDefault(key, def);
	}

	@SuppressWarnings("unchecked")
	public boolean[] getBooleanArray(@NonNls String key, boolean[] def) {
		List<Boolean> list = (List<Boolean>) map.get(key);
		if (list == null)
			return def;
		boolean[] array = new boolean[list.size()];
		for (int i = 0; i < list.size(); i++)
			array[i] = list.get(i);
		return array;
	}

	@SuppressWarnings("unchecked")
	public DataObject getObject(@NonNls String key, DataObject def) {
		Map<String, Object> entry = (Map<String, Object>) map.get(key);
		if (entry == null)
			return def;
		return new DataObject(entry);
	}

	@SuppressWarnings("unchecked")
	public DataObject[] getObjectArray(@NonNls String key, DataObject[] def) {
		List<Map<String, Object>> entry = (List<Map<String, Object>>) map.get(key);
		if (entry == null)
			return def;
		return entry.stream().map(DataObject::new).toArray(DataObject[]::new);
	}

	public void setString(@NonNls String key, String value) {
		map.put(key, value);
	}

	public void setStringArray(@NonNls String key, String[] value) {
		map.put(key, Arrays.asList(value));
	}

	void setInt(@NonNls String key, int value) {
		map.put(key, value);
	}

	public void setIntArray(@NonNls String key, int[] value) {
		List<Integer> list = new ArrayList<>(value.length);
		for (int i : value)
			list.add(i);
		map.put(key, list);
	}

	public void setDouble(@NonNls String key, double value) {
		map.put(key, value);
	}

	public void setDoubleArray(@NonNls String key, double[] value) {
		List<Double> list = new ArrayList<>(value.length);
		for (double i : value)
			list.add(i);
		map.put(key, list);
	}

	public void setFloat(@NonNls String key, float value) {
		map.put(key, value);
	}

	public void setFloatArray(@NonNls String key, float[] value) {
		List<Float> list = new ArrayList<>(value.length);
		for (float i : value)
			list.add(i);
		map.put(key, list);
	}

	public void setBoolean(@NonNls String key, boolean value) {
		map.put(key, value);
	}

	public void setBooleanArray(@NonNls String key, boolean[] value) {
		List<Boolean> list = new ArrayList<>(value.length);
		for (boolean i : value)
			list.add(i);
		map.put(key, list);
	}

	public void setObject(@NonNls String key, DataObject value) {
		map.put(key, value.asMap());
	}

	public void setObjectArray(@NonNls String key, DataObject[] value) {
		List<Map<String, Object>> list = new ArrayList<>(value.length);
		for (DataObject object : value) {
			list.add(object.asMap());
		}
		map.put(key, list);
	}

	public void setArray(@NonNls String key, DataArray value) {
		map.put(key, value.asList());
	}

	@SuppressWarnings("unchecked")
	public DataArray getArray(@NonNls String key) {
		return new DataArray((List<Object>) map.get(key));
	}

}
