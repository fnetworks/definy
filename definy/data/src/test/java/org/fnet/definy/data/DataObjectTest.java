package org.fnet.definy.data;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class DataObjectTest {

	@Test
	public void SetGetString() {
		DataObject object = new DataObject();
		object.setString("test", "value");
		assertEquals("value", object.getString("test"));
	}

	@Test
	public void SetGetStringArray() {
		DataObject object = new DataObject();
		object.setStringArray("test", new String[]{"value1", "value2"});
		assertArrayEquals(new String[]{"value1", "value2"}, object.getStringArray("test"));
	}

	@Test
	public void SetGetInt() {
		DataObject object = new DataObject();
		object.setInt("test", 42);
		assertEquals(42, object.getInt("test"));
	}

	@Test
	public void SetGetIntArray() {
		DataObject object = new DataObject();
		object.setIntArray("test", new int[]{42, 123});
		assertArrayEquals(new int[]{42, 123}, object.getIntArray("test"));
	}

	@Test
	public void SetGetFloat() {
		DataObject object = new DataObject();
		object.setFloat("test", 42.6f);
		assertEquals(42.6f, object.getFloat("test"), 0);
	}

	@Test
	public void SetGetFloatArray() {
		DataObject object = new DataObject();
		object.setFloatArray("test", new float[]{42.6f, 123f});
		assertArrayEquals(new float[]{42.6f, 123f}, object.getFloatArray("test"), 0);
	}

	@Test
	public void SetGetDouble() {
		DataObject object = new DataObject();
		object.setDouble("test", 42.6d);
		assertEquals(42.6d, object.getDouble("test"), 0);
	}

	@Test
	public void SetGetDoubleArray() {
		DataObject object = new DataObject();
		object.setDoubleArray("test", new double[]{42.6d, 123d});
		assertArrayEquals(new double[]{42.6d, 123d}, object.getDoubleArray("test"), 0);
	}

	@Test
	public void SetGetBoolean() {
		DataObject object = new DataObject();
		object.setBoolean("test", true);
		assertEquals(true, object.getBoolean("test"));
	}

	@Test
	public void SetGetBooleanArray() {
		DataObject object = new DataObject();
		object.setBooleanArray("test", new boolean[]{true, false});
		assertArrayEquals(new boolean[]{true, false}, object.getBooleanArray("test"));
	}

}