package org.fnet.definy.ui.property;

public class ModifyEvent<T> {

	private T previous, current;

	ModifyEvent(T previous, T current) {
		this.previous = previous;
		this.current = current;
	}

	public T getPrevious() {
		return previous;
	}

	public T getCurrent() {
		return current;
	}
}
