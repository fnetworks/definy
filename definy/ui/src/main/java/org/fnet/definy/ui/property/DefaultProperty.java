package org.fnet.definy.ui.property;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DefaultProperty<T> implements Property<T> {

	private T value;
	private List<PropertyChangeListener<T>> propertyChangeListeners = new ArrayList<>();

	public DefaultProperty(T initialValue) {
		this.value = initialValue;
	}

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public void setValue(T value) {
		if (Objects.equals(this.value, value))
			return;
		T initialValue = this.value;
		this.value = value;
		ModifyEvent<T> event = new ModifyEvent<>(initialValue, value);
		propertyChangeListeners.forEach(e -> e.onPropertyChanged(event));
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener<T> changeListener) {
		if (changeListener == null)
			throw new NullPointerException("changeListener");
		propertyChangeListeners.add(changeListener);
	}

	@Override
	public boolean removePropertyChangeListener(PropertyChangeListener<T> listener) {
		return propertyChangeListeners.remove(listener);
	}

}
