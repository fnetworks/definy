package org.fnet.definy.ui;

import org.fnet.definy.ui.property.ModifyEvent;

public interface ModifyEventListener {

	void onModified(ModifyEvent e);

}
