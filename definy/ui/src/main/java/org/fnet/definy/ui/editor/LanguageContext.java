package org.fnet.definy.ui.editor;

import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fnet.definy.data.DataObject;
import org.fnet.definy.data.DataSerializers;
import org.fnet.definy.data.Loadable;
import org.fnet.definy.project.SourceFile;
import org.fnet.definy.project.compiler.ILinter;
import org.fnet.definy.ui.EditorPanel;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.Nullable;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.regex.Pattern;

public class LanguageContext implements Loadable, EditorProvider {

	@NonNls
	private static final String CONSTANT_SYNTAX_PREFIX = "const:";

	private String name;
	private Pattern[] globs;
	private String syntaxStyle;
	private CompletionContext completionContext;

	@Nullable
	private ILinter linter;

	public LanguageContext(String name, Pattern[] globs, String syntaxStyle, CompletionContext completionContext) {
		this.name = name;
		this.globs = globs;
		this.syntaxStyle = syntaxStyle;
		this.completionContext = completionContext;
	}

	public LanguageContext(InputStream in) throws IOException {
		load(DataSerializers.deserialize("json", in));
	}

	@Override
	public void load(DataObject data) {
		name = data.getString("name");
		globs = Arrays.stream(data.getStringArray("globs")).map(Pattern::compile).toArray(Pattern[]::new);
		String syntaxStyle = data.getString("syntax");
		if (syntaxStyle.startsWith(CONSTANT_SYNTAX_PREFIX)) {
			syntaxStyle = syntaxStyle.substring(CONSTANT_SYNTAX_PREFIX.length());
			try {
				this.syntaxStyle = (String) SyntaxConstants.class.getField("SYNTAX_STYLE_" + syntaxStyle).get(null);
			} catch (NoSuchFieldException | IllegalAccessException e) {
				LoggerFactory.getLogger(LanguageContext.class).error("Can't access syntax style " + syntaxStyle, e);
			}
		} else {
			this.syntaxStyle = syntaxStyle;
		}
		this.completionContext = new CompletionContext();
		completionContext.load(data.getObject("completion"));
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean canEdit(File file) {
		for (Pattern glob : globs)
			if (glob.matcher(file.getName()).matches())
				return true;
		return false;
	}

	@Override
	public JComponent createEditor(SourceFile file) throws IOException {
		return new EditorPanel(this, file, syntaxStyle);
	}

	@Override
	public JComponent createEditor(File file) throws IOException {
		return new EditorPanel(this, file, syntaxStyle);
	}

	public Pattern[] getGlobs() {
		return globs;
	}

	public String getSyntaxStyle() {
		return syntaxStyle;
	}

	public CompletionContext getCompletionContext() {
		return completionContext;
	}

	@Nullable
	public ILinter getLinter() {
		return linter;
	}

	public void setLinter(@Nullable ILinter linter) {
		this.linter = linter;
	}
}
