package org.fnet.definy.ui;

import com.alee.extended.syntax.WebSyntaxArea;
import com.alee.extended.syntax.WebSyntaxScrollPane;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.parser.AbstractParser;
import org.fife.ui.rsyntaxtextarea.parser.DefaultParseResult;
import org.fife.ui.rsyntaxtextarea.parser.DefaultParserNotice;
import org.fife.ui.rsyntaxtextarea.parser.ParseResult;
import org.fnet.definy.project.SourceFile;
import org.fnet.definy.project.compiler.CompilerMessage;
import org.fnet.definy.project.components.LintComponent;
import org.fnet.definy.ui.editor.EditorProvider;
import org.fnet.definy.ui.editor.IEditable;
import org.fnet.definy.ui.editor.LanguageContext;
import org.fnet.definy.ui.property.DefaultProperty;
import org.fnet.definy.ui.property.Property;
import org.fnet.definy.ui.util.DocumentAdapter;
import org.fnet.definy.util.CommonDirectories;
import org.jetbrains.annotations.Nullable;
import org.slf4j.LoggerFactory;

import javax.swing.event.DocumentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

public class EditorPanel extends WebSyntaxScrollPane implements IEditable {

	private WebSyntaxArea textArea;
	private final Property<Boolean> modified = new DefaultProperty<>(false);
	private final EditorProvider provider;
	private final String syntaxStyle;
	private final File source;
	@Nullable
	private final SourceFile sourceFile;

	public EditorPanel(EditorProvider provider, File source, String syntaxStyle) throws IOException {
		this.provider = provider;
		this.source = source;
		this.syntaxStyle = syntaxStyle;
		this.sourceFile = null;

		createUI();
	}

	public EditorPanel(EditorProvider provider, SourceFile sourceFile, String syntaxStyle) throws IOException {
		this.provider = provider;
		this.source = sourceFile.getFile();
		this.sourceFile = sourceFile;
		this.syntaxStyle = syntaxStyle;

		createUI();
	}

	private void createUI() throws IOException {
		this.textArea = new WebSyntaxArea(20, 60);
		this.textArea.setSyntaxEditingStyle(syntaxStyle);
		this.textArea.setCodeFoldingEnabled(true);

		if (sourceFile != null && sourceFile.getModule().hasComponent(LintComponent.class)) {
			textArea.addParser(new AbstractParser() {
				@Override
				public ParseResult parse(RSyntaxDocument doc, String style) {
					DefaultParseResult res = new DefaultParseResult(this);
					try {
						File file;
						if (isModified()) {
							file = new File(CommonDirectories.getTempDirectory(), "lint_" + source.getName());
							Files.write(source.toPath(), textArea.getText().getBytes(StandardCharsets.UTF_8));
						} else {
							file = source;
						}
						List<CompilerMessage> lint = sourceFile.getModule().getComponent(LintComponent.class).lint(file);
						for (CompilerMessage msg : lint) {
							res.addNotice(new DefaultParserNotice(this, msg.getMessage(), msg.getLine()));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					return res;
				}
			});
		}

		AutoCompletion completion = new AutoCompletion(createCompletions());
		completion.install(textArea);

		this.textArea.setText(new String(Files.readAllBytes(source.toPath()), StandardCharsets.UTF_8));

		textArea.getDocument().addDocumentListener(new DocumentAdapter() {
			@Override
			public void changedUpdate(DocumentEvent e) {
				modifiedProperty().setValue(true);
			}
		});

		textArea.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_S && (e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0) {
					try {
						save(source);
					} catch (IOException e1) {
						LoggerFactory.getLogger(EditorPanel.class).error("Failed to save file", e1);
					}
				}
			}
		});

		setViewportView(textArea);
	}

	private CompletionProvider createCompletions() {
		DefaultCompletionProvider provider = new DefaultCompletionProvider();
		if (this.provider instanceof LanguageContext) {
			LanguageContext ctx = (LanguageContext) this.provider;
			for (String basic : ctx.getCompletionContext().getBasicCompletions())
				provider.addCompletion(new BasicCompletion(provider, basic));
		}
		return provider;
	}

	public void setSyntaxStyle(String syntaxStyle) {
		this.textArea.setSyntaxEditingStyle(syntaxStyle);
	}

	public void setContent(String content) {
		this.textArea.setText(content);
	}

	public String getContent() {
		return this.textArea.getText();
	}

	public WebSyntaxArea getTextArea() {
		return textArea;
	}

	public EditorProvider getProvider() {
		return provider;
	}

	public String getSyntaxStyle() {
		return syntaxStyle;
	}

	public File getSource() {
		return source;
	}

	public SourceFile getSourceFile() {
		return sourceFile;
	}

	@Override
	public void save(File source) throws IOException {
		Files.write(source.toPath(), textArea.getText().getBytes(StandardCharsets.UTF_8));
		modifiedProperty().setValue(false);
	}

	@Override
	public Property<Boolean> modifiedProperty() {
		return modified;
	}
}
