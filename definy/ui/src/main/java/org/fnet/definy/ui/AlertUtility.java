package org.fnet.definy.ui;

import com.alee.laf.optionpane.WebOptionPane;

import java.awt.*;

public class AlertUtility {

	public static void showAlert(Component parentComponent, Exception exception, String title) {
		StringBuilder message = new StringBuilder();
		message.append(exception.getClass().getSimpleName());
		if (exception.getMessage() != null && !exception.getMessage().isEmpty())
			message.append(": ").append(exception.getMessage());
		WebOptionPane.showMessageDialog(parentComponent, message.toString(), title, WebOptionPane.ERROR_MESSAGE);
	}

	public static void showAlert(Component parentComponent, String error, String title) {
		WebOptionPane.showMessageDialog(parentComponent, error, title, WebOptionPane.ERROR_MESSAGE);
	}

}
