package org.fnet.definy.ui;

import java.util.EventListener;

public interface CloseEventListener extends EventListener {

	void onClose();

}
