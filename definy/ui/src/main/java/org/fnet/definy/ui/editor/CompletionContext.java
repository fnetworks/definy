package org.fnet.definy.ui.editor;

import org.fnet.definy.data.DataObject;
import org.fnet.definy.data.Loadable;

public class CompletionContext implements Loadable {

	private String[] basicCompletions;

	public CompletionContext(String[] basicCompletions) {
		this.basicCompletions = basicCompletions;
	}

	CompletionContext() {
	}

	@Override
	public void load(DataObject data) {
		basicCompletions = data.getStringArray("basic");
	}

	public String[] getBasicCompletions() {
		return basicCompletions;
	}
}
