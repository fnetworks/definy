package org.fnet.definy.ui.tabs;

import com.alee.laf.panel.WebPanel;
import org.fnet.definy.ui.editor.IEditable;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class EditorTabContentPanel extends WebPanel {

	private JComponent content;
	private File source;

	public EditorTabContentPanel(File source, JComponent content) {
		this.source = source;
		this.content = content;
		setLayout(new BorderLayout());
		add(content, BorderLayout.CENTER);
	}

	public JComponent getContent() {
		return content;
	}

	/**
	 * @return The content as an IEditable, or null if the component isn't editable
	 */
	public IEditable getEditableContent() {
		if (content instanceof IEditable)
			return (IEditable) content;
		return null;
	}

	public File getSource() {
		return source;
	}

	public EditorTabTitlePanel getTabTitleComponent(JTabbedPane pane) {
		return (EditorTabTitlePanel) pane.getTabComponentAt(pane.indexOfComponent(this));
	}

	public boolean save() throws IOException {
		if (content instanceof IEditable) {
			((IEditable) content).save(getSource());
			return true;
		} else {
			return false;
		}
	}

}
