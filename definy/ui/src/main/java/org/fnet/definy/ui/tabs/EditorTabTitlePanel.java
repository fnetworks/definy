package org.fnet.definy.ui.tabs;

import com.alee.laf.button.WebButton;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import org.fnet.definy.lang.Localizer;
import org.fnet.definy.ui.editor.IEditable;
import org.jetbrains.annotations.NonNls;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class EditorTabTitlePanel extends WebPanel {

	private boolean changed = false;

	public EditorTabTitlePanel(EditorTabPane pane, EditorTabContentPanel content) {
		super(new FlowLayout(FlowLayout.LEADING, 0, 0));
		setOpaque(false);

		JLabel titleLabel = new JLabel() {
			@Override
			public String getText() {
				int i = pane.getComponent().indexOfTabComponent(EditorTabTitlePanel.this);
				if (i != -1)
					return pane.getComponent().getTitleAt(i) + (changed ? " *" : "");
				return null;
			}
		};
		titleLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
		add(titleLabel);
		IEditable editable = content.getEditableContent();
		if (editable != null) {
			editable.modifiedProperty().addPropertyChangeListener(event -> {
				setChanged(event.getCurrent());
			});
		}

		@NonNls
		WebButton button = new CloseButton();
		button.addActionListener(e -> {
			boolean close = true;
			if (this.changed) {
				int option = WebOptionPane.showConfirmDialog(pane.getComponent(), Localizer.getString("editortabtitlepane.savefile.question"),
						Localizer.getString("editortabtitlepane.savefile.title"),
						WebOptionPane.YES_NO_CANCEL_OPTION);
				switch (option) {
					case WebOptionPane.YES_OPTION:
						try {
							((EditorTabContentPanel) pane.getComponent().getComponentAt(pane.getComponent().indexOfTabComponent(EditorTabTitlePanel.this))).save();
						} catch (IOException e1) {
							LoggerFactory.getLogger(getClass()).error("Failed to save file", e1);
							int errOption = WebOptionPane.showConfirmDialog(pane.getComponent(), Localizer.getString("editortabtitlepane.savefile.failed"),
									Localizer.getString("editortabtitlepane.savefile.failed.title"), WebOptionPane.YES_NO_OPTION, WebOptionPane.ERROR_MESSAGE);
							if (errOption == WebOptionPane.NO_OPTION)
								close = false;
						}
						break;
					case WebOptionPane.NO_OPTION:
						break;
					case WebOptionPane.CANCEL_OPTION:
						close = false;
						break;
				}
			}
			if (close)
				pane.removeTab(content);
		});
		add(button);

		setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
	}

	public EditorTabContentPanel getTabContentPanel(JTabbedPane pane) {
		return (EditorTabContentPanel) pane.getComponentAt(pane.indexOfTabComponent(this));
	}

	public boolean isChanged() {
		return changed;
	}

	public void setChanged(boolean changed) {
		boolean before = this.changed;
		this.changed = changed;
		if (changed != before) {
			revalidate();
			repaint();
		}
	}
}
