package org.fnet.definy.ui;

import com.alee.laf.window.WebDialog;
import org.fnet.definy.project.Project;

import javax.swing.*;
import java.awt.*;

public class ProjectSettingsDialog extends WebDialog {

	private UIPropertiesEditor propertiesEditor;

	public ProjectSettingsDialog(Project project) {
		setTitle("Project settings");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setMinimumSize(new Dimension(400, 240));

		this.propertiesEditor = new UIPropertiesEditor(project);
		add(new JScrollPane(propertiesEditor));
	}

}
