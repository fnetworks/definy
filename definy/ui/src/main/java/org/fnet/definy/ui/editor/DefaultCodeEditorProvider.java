package org.fnet.definy.ui.editor;

import org.fnet.definy.project.SourceFile;
import org.fnet.definy.ui.EditorPanel;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

public class DefaultCodeEditorProvider implements EditorProvider {

	private String name;
	private String glob;
	private String syntaxStyle;

	public DefaultCodeEditorProvider(String name, String glob, String syntaxStyle) {
		this.name = name;
		this.glob = glob;
		this.syntaxStyle = syntaxStyle;
	}

	private transient Pattern _globPattern;

	private Pattern getGlobPattern() {
		if (_globPattern == null)
			_globPattern = Pattern.compile(glob);
		return _globPattern;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean canEdit(File file) {
		return getGlobPattern().matcher(file.getName()).matches();
	}

	@Override
	public JComponent createEditor(File file) throws IOException {
		return new EditorPanel(this, file, syntaxStyle);
	}

	@Override
	public JComponent createEditor(SourceFile file) throws IOException {
		return new EditorPanel(this, file, syntaxStyle);
	}
}
