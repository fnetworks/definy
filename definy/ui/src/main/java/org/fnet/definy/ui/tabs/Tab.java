package org.fnet.definy.ui.tabs;

import javax.swing.*;
import java.io.File;

public class Tab {

	private JTabbedPane parentPane;
	private File file;
	private EditorTabContentPanel content;
	private EditorTabTitlePanel title;

	public Tab(JTabbedPane parentPane, File file, EditorTabTitlePanel title, EditorTabContentPanel content) {
		this.parentPane = parentPane;
		this.file = file;
		this.content = content;
		this.title = title;
	}

	public void addToPane(boolean focus) {
		parentPane.addTab(file.getName(), content);
		int index = parentPane.indexOfComponent(content);
		parentPane.setTabComponentAt(index, title);
		if (focus)
			parentPane.setSelectedIndex(index);
	}

	public void focus() {
		parentPane.setSelectedComponent(content);
	}

	public EditorTabContentPanel getContent() {
		return content;
	}

	public EditorTabTitlePanel getTitle() {
		return title;
	}
}
