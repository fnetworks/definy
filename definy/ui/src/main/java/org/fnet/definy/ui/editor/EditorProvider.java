package org.fnet.definy.ui.editor;

import org.fnet.definy.project.SourceFile;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public interface EditorProvider {

	String getName();

	boolean canEdit(File file);

	default JComponent createEditor(SourceFile file) throws IOException {
		return createEditor(file.getFile());
	}

	JComponent createEditor(File file) throws IOException;

}
