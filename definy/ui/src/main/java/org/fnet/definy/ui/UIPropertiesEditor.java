package org.fnet.definy.ui;

import de.sciss.treetable.j.DefaultTreeColumnModel;
import de.sciss.treetable.j.DefaultTreeTableNode;
import de.sciss.treetable.j.TreeTable;
import org.fnet.definy.project.Property;
import org.slf4j.LoggerFactory;

import javax.swing.tree.DefaultTreeModel;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.BiFunction;

public class UIPropertiesEditor extends TreeTable {

	private final DefaultTreeModel model;
	private final DefaultTreeTableNode root;
	private final DefaultTreeColumnModel columnModel;

	public UIPropertiesEditor(Object o) {
		setShowsRootHandles(true);
		setAutoCreateRowHeader(true);

		root = new DefaultTreeTableNode("Key", "Value");
		model = new DefaultTreeModel(root);
		setTreeModel(model);

		columnModel = new DefaultTreeColumnModel(root, "Key", "Value");
		setTreeColumnModel(columnModel);

		try {
			root.add(createNode("Root", o, new LinkedList<>()));
		} catch (IllegalAccessException e) {
			LoggerFactory.getLogger(UIPropertiesEditor.class).error("Error while reading class properties", e);
		}

		System.out.println();
	}

	private static final Map<Class<?>, BiFunction<String, Object, DefaultTreeTableNode>> MAPPERS = new HashMap<>();

	@SuppressWarnings("unchecked")
	private static <T> void addMapper(Class<T> cls, BiFunction<String, T, DefaultTreeTableNode> mapper) {
		MAPPERS.put(cls, (BiFunction<String, Object, DefaultTreeTableNode>) mapper);
	}

	static {
		addMapper(String.class, (name, object) -> new DefaultTreeTableNode(name, object));
		addMapper(File.class, (name, object) -> new DefaultTreeTableNode(name, object.getPath()));
		addMapper(Enum.class, (name, object) -> new DefaultTreeTableNode(name, object.name()));
	}

	// TODO type specific editing

	private DefaultTreeTableNode createNode(String name, Object object, Deque<Object> objects) throws IllegalAccessException {
		if (object == null)
			return new DefaultTreeTableNode(name, "null");

		for (Object o : objects)
			if (o == object)
				return new DefaultTreeTableNode("RECURSIVE REFERENCE", "");

		if (object.getClass().isPrimitive())
			return new DefaultTreeTableNode(name, object);

		for (Map.Entry<Class<?>, BiFunction<String, Object, DefaultTreeTableNode>> entry : MAPPERS.entrySet())
			if (entry.getKey().isInstance(object))
				return entry.getValue().apply(name, object);

		if (object.getClass().isArray()) { // TODO List
			DefaultTreeTableNode node = new DefaultTreeTableNode(name, "[]");
			Object[] array = (Object[]) object;
			objects.push(object);
			for (int i = 0; i < array.length; i++) {
				Object o = array[i];
				node.add(createNode(i + "", o, objects));
			}
			objects.pop();
			return node;
		} else {
			DefaultTreeTableNode node = new DefaultTreeTableNode(name, object.getClass().getSimpleName());
			objects.push(object);
			for (Field f : getAllFields(object.getClass())) {
				if (Modifier.isStatic(f.getModifiers()))
					continue;
				if (!f.isAnnotationPresent(Property.class))
					continue;
				node.add(createNode(f, object, objects));
			}
			objects.pop();
			return node;
		}
	}

	private List<Field> getAllFields(Class<?> cls) {
		List<Field> fields = new ArrayList<>(Arrays.asList(cls.getDeclaredFields()));
		if (cls.getSuperclass() != null)
			fields.addAll(getAllFields(cls.getSuperclass()));
		return fields;
	}

	private DefaultTreeTableNode createNode(Field field, Object instance, Deque<Object> objects) throws
			IllegalAccessException {
		field.setAccessible(true);
		return createNode(field.getName(), field.get(instance), objects);
	}

}
