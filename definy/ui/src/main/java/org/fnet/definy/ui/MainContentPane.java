package org.fnet.definy.ui;

import com.alee.extended.tree.AsyncTreeAdapter;
import com.alee.extended.tree.AsyncUniqueNode;
import com.alee.extended.tree.FileTreeNode;
import com.alee.extended.tree.WebFileTree;
import com.alee.laf.button.WebButton;
import com.alee.laf.optionpane.WebOptionPane;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.tabbedpane.WebTabbedPane;
import com.alee.laf.toolbar.WebToolBar;
import com.alee.managers.style.StyleId;
import org.fnet.definy.data.DataArray;
import org.fnet.definy.data.DataObject;
import org.fnet.definy.data.DataSerializers;
import org.fnet.definy.data.Persistable;
import org.fnet.definy.lang.Localizer;
import org.fnet.definy.project.Module;
import org.fnet.definy.project.Project;
import org.fnet.definy.project.SourceFile;
import org.fnet.definy.project.components.SourceFileComponent;
import org.fnet.definy.ui.tabs.EditorTabPane;
import org.fnet.definy.util.FileUtil;
import org.fnet.definy.util.Util;
import org.fnet.definy.util.streamprinter.PrintTarget;
import org.fnet.definy.util.streamprinter.TextType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MainContentPane extends WebPanel {

	private final EditorTabPane editorTabs;
	private final WebTabbedPane bottomTabs;
	private final AppendableTextPane consoleTextPane;
	private final WebFileTree fileTree;
	private final WebToolBar toolBar;
	private final Project project;

	private final Logger logger = LoggerFactory.getLogger(MainContentPane.class);

	public MainContentPane(Project root) {
		super(new BorderLayout());

		this.project = root;

		this.editorTabs = new EditorTabPane();
		this.add(this.editorTabs.getComponent(), BorderLayout.CENTER);

		this.toolBar = new WebToolBar(StyleId.toolbarAttachedNorth);
		this.toolBar.setFloatable(false);

		this.add(toolBar, BorderLayout.PAGE_START);

		this.bottomTabs = new WebTabbedPane();
		bottomTabs.setPreferredSize(new Dimension(0, 300));
		this.add(bottomTabs, BorderLayout.PAGE_END);

		this.consoleTextPane = new AppendableTextPane();
		consoleTextPane.setEditable(false);
		bottomTabs.addTab(Localizer.getString("maincontentpane.bottomtabs.console.title"), new WebScrollPane(consoleTextPane));

		this.fileTree = new WebFileTree(root.getLocation());
		this.add(this.fileTree, BorderLayout.LINE_START);
		this.fileTree.setPreferredSize(new Dimension(300, 0));

		this.fileTree.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				showPopup(e);
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				showPopup(e);
			}

			private void showPopup(MouseEvent e) {
				if (!e.isPopupTrigger())
					return;
				TreePath nodePath = fileTree.getPathForLocation(e.getPoint());
				if (nodePath == null)
					return;
				FileTreeNode node = fileTree.getNodeForPath(nodePath);
				JPopupMenu menu = new JPopupMenu();
				constructPopupMenu(menu, node);
				menu.show(e.getComponent(), e.getX(), e.getY());
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					if (e.getClickCount() == 2) {
						File selectedFile = fileTree.getSelectedFile();
						if (selectedFile != null && !selectedFile.isDirectory()) {
							try {
								openFile(selectedFile);
							} catch (IOException e1) {
								AlertUtility.showAlert(MainContentPane.this, e1, Localizer.getString("maincontentpane.error.createeditor"));
								logger.error("Error while creating editor", e1);
							}
						}
					}
				}
			}
		});

		this.fileTree.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DELETE) {
					requestDeleteFileTreeNode(fileTree.getSelectedNode());
				}
			}
		});

		PrintTarget consolePrintTarget = (string, type) ->
				consoleTextPane.appendColored(string + "\n", type == TextType.STDERR ? Color.RED : Color.BLACK);

		addToolBarButton(Localizer.getString("maincontentpane.toolbar.build"), new AsyncAction("Build thread", e -> {
					try {
						root.build(consolePrintTarget);
					} catch (Exception e1) {
						SwingUtilities.invokeLater(() ->
								AlertUtility.showAlert(this, e1, Localizer.getString("maincontentpane.toolbar.build.error", e1.getLocalizedMessage())));
						logger.error("Error while building project", e1);
					}
				})
		);

		addToolBarButton(Localizer.getString("maincontentpane.toolbar.run"), new AsyncAction("Run thread", e -> {
			try {
				root.run(consolePrintTarget);
			} catch (Exception e1) {
				SwingUtilities.invokeLater(() ->
						AlertUtility.showAlert(this, e1, Localizer.getString("maincontentpane.toolbar.run.error", e1.getLocalizedMessage())));
				logger.error("Error while running project", e1);
			}
		}));

		try {
			FileWatcher fileWatcher = new FileWatcher(root.getLocation());
			fileWatcher.addFileChangeListener(file -> SwingUtilities.invokeLater(() -> reload(file)));
			fileWatcher.addFileChangeListener(file -> {
				Module module = getModuleOfFile(file);
				if (module != null && module.hasComponent(SourceFileComponent.class)) {
					SourceFileComponent sfComponent = module.getComponent(SourceFileComponent.class);
					for (File sourceDir : sfComponent.getSourceDirectories()) {
						if (file.toPath().startsWith(sourceDir.toPath())) {
							sfComponent.scanSourceFiles(); // TODO only scan source files for specific source directory
							return;
						}
					}
				}
			});
			fileWatcher.start();
		} catch (IOException | UnsupportedOperationException e) {
			logger.error("Could not watch directory", e);
		}
	}

	private void openFile(File selectedFile) throws IOException {
		Module module = getModuleOfFile(selectedFile);
		if (module != null && module.hasComponent(SourceFileComponent.class)) {
			SourceFile sourceFile = module.getComponent(SourceFileComponent.class).getSourceFile(selectedFile);
			if (sourceFile != null) {
				editorTabs.createTabFromFile(sourceFile);
			} else {
				editorTabs.createTabFromFile(selectedFile);
			}
		} else {
			editorTabs.createTabFromFile(selectedFile);
		}
	}

	private Module getModuleOfFile(File file) {
		for (Module m : project.getModules())
			if (file.toPath().startsWith(m.getLocation().toPath()))
				return m;
		return null;
	}

	private static class AsyncAction implements ActionListener {

		private String name;
		private ActionListener listener;

		public AsyncAction(String name, ActionListener listener) {
			this.name = name;
			this.listener = listener;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			new Thread(() -> listener.actionPerformed(e), name).start();
		}
	}

	private void constructPopupMenu(JPopupMenu menu, FileTreeNode node) {
		JMenu addMenu = new JMenu(Localizer.getString("maincontentpane.menu.new"));
		menu.add(addMenu);

		JMenuItem newFileItem = new JMenuItem(Localizer.getString("maincontentpane.menu.new.file"));
		addMenu.add(newFileItem);
		newFileItem.addActionListener(e -> {
			File directory = node.getFile().isDirectory() ? node.getFile() : node.getFile().getParentFile();
			String name = WebOptionPane.showInputDialog(this, "Enter a name for the new file");
			if (name == null)
				return;
			File newFile = new File(directory, name);
			if (newFile.exists()) {
				WebOptionPane.showMessageDialog(this, "File already exists", "Error creating file", WebOptionPane.ERROR_MESSAGE);
				return;
			}
			try {
				if (!newFile.createNewFile())
					throw new IOException(String.format("Unknown error creating file %s", newFile));
			} catch (IOException e1) {
				logger.error("Error creating file", e1);
				AlertUtility.showAlert(this, e1, "Error creating file");
				return;
			}
			try {
				editorTabs.createTabFromFile(newFile);
			} catch (IOException e1) {
				logger.error("Failed to create tab", e1);
			}
			reload(directory);
		});

		JMenuItem newDirectoryItem = new JMenuItem(Localizer.getString("maincontentpane.menu.new.directory"));
		addMenu.add(newDirectoryItem);
		newDirectoryItem.addActionListener(e -> {
			File directory = node.getFile().isDirectory() ? node.getFile() : node.getFile().getParentFile();
			String name = WebOptionPane.showInputDialog(this, "Enter a name for the new directory");
			if (name == null)
				return;
			File newDir = new File(directory, name);
			if (newDir.exists()) {
				WebOptionPane.showMessageDialog(this, "Directory already exists", "Error creating directory", WebOptionPane.ERROR_MESSAGE);
				return;
			}
			try {
				if (!newDir.mkdir())
					throw new IOException(String.format("Unknown error creating directory %s", newDir));
			} catch (IOException e1) {
				logger.error("Error creating file", e1);
				AlertUtility.showAlert(this, e1, "Error creating file");
			}
		});

		JMenuItem renameItem = new JMenuItem(Localizer.getString("maincontentpane.menu.rename"));
		renameItem.addActionListener(e -> requestMoveFileTreeNode(node));
		menu.add(renameItem);

		JMenuItem deleteItem = new JMenuItem(Localizer.getString("maincontentpane.menu.delete"));
		menu.add(deleteItem);
		deleteItem.addActionListener(e -> requestDeleteFileTreeNode(node));
	}

	private void requestMoveFileTreeNode(FileTreeNode node) {
		String name = FileUtil.relative(node.getFile(), project.getLocation());
		String targetName = (String) WebOptionPane.showInputDialog(this, "Enter new name", "Rename " + name,
				WebOptionPane.QUESTION_MESSAGE, null, null, node.getFile().getName());
		if (targetName != null) {
			try {
				FileUtil.rename(node.getFile(), targetName);
			} catch (IOException e) {
				logger.error("Error renaming file", e);
				AlertUtility.showAlert(this, e, "Error renaming file");
			}
		}
	}

	private void requestDeleteFileTreeNode(FileTreeNode node) {
		String name = FileUtil.relative(node.getFile(), project.getLocation());
		if (WebOptionPane.showConfirmDialog(this, "Delete " + name + "?", "Delete file",
				JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
			try {
				FileUtil.delete(node.getFile());
				reload(node.getFile());
			} catch (IOException e) {
				logger.error("Error deleting file", e);
				AlertUtility.showAlert(this, e, "Could not delete file"); // TODO localize
			}
		}
	}

	private class NodeState implements Persistable {
		private File nodeFile;
		private List<NodeState> children;

		private NodeState(FileTreeNode node) {
			this.nodeFile = node.getFile();
			this.children = Util.enumerationAsStream(node.children())
					.filter(fileTree::isExpanded)
					.map(NodeState::new)
					.collect(Collectors.toList());
		}

		private NodeState() {
			children = new ArrayList<>();
		}

		public void apply() {
			fileTree.expandNode(fileTree.getNode(nodeFile));
			children.forEach(NodeState::apply);
		}

		@Override
		public void save(DataObject data) {
			data.setString("file", nodeFile.getAbsolutePath());
			DataArray childDatas = new DataArray();
			for (NodeState child : children) {
				DataObject childData = new DataObject();
				child.save(childData);
				childDatas.addObject(childData);
			}
			data.setArray("children", childDatas);
		}

		@Override
		public void load(DataObject data) {
			nodeFile = new File(data.getString("file"));
			DataArray childDatas = data.getArray("children");
			for (DataObject object : childDatas.asObjectList()) {
				NodeState state = new NodeState();
				state.load(object);
				children.add(state);
			}
		}
	}

	private void reload(File file) {
		FileTreeNode parentNode = fileTree.getNode(file.getParentFile());
		if (parentNode == null)
			return; // Node not found or not loaded

		NodeState nodeState = new NodeState(parentNode);
		fileTree.addAsyncTreeListener(new AsyncTreeAdapter() {
			@Override
			public void loadCompleted(AsyncUniqueNode parent, List children) {
				nodeState.apply();
				fileTree.removeAsyncTreeListener(this);
			}

			@Override
			public void loadFailed(AsyncUniqueNode parent, Throwable cause) {
				logger.warn("Load failed: " + parent + ", ", cause);
				fileTree.removeAsyncTreeListener(this);
			}
		});
		fileTree.reloadNode(parentNode);
	}

	private void addToolBarButton(String name, ActionListener actionListener) {
		WebButton button = new WebButton(name);
		button.addActionListener(actionListener);
		this.toolBar.addToEnd(button);
	}

	private DataObject saveEditorState() {
		DataObject data = new DataObject();
		data.setStringArray("tabs", editorTabs.getTabs().stream()
				.map(e -> e.getContent().getSource().getAbsolutePath()).toArray(String[]::new));
		NodeState nodeState = new NodeState(fileTree.getRootNode());
		DataObject nodeStateData = new DataObject();
		nodeState.save(nodeStateData);
		data.setObject("nodeState", nodeStateData);
		return data;
	}

	public void writeEditorStateToProject() throws IOException {
		FileUtil.mkdirs(project.getMetadataDirectory());
		File editorStateFile = new File(project.getMetadataDirectory(), "editorState.json");
		DataSerializers.serialize(saveEditorState(), editorStateFile);
	}

	private void restoreEditorState(DataObject data) {
		for (String tab : data.getStringArray("tabs")) {
			try {
				openFile(new File(tab));
			} catch (IOException e) {
				logger.error("Could not reopen tab", e);
			}
		}
		NodeState state = new NodeState();
		state.load(data.getObject("nodeState"));
		state.apply();
	}

	public void restoreEditorStateFromProject() throws IOException {
		File editorStateFile = new File(project.getMetadataDirectory(), "editorState.json");
		if (editorStateFile.exists()) {
			restoreEditorState(DataSerializers.deserialize(editorStateFile));
		}
	}

}
