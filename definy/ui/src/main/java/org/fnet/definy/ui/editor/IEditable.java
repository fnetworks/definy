package org.fnet.definy.ui.editor;

import org.fnet.definy.ui.property.Property;

import java.io.File;
import java.io.IOException;

public interface IEditable {

	void save(File source) throws IOException;

	default boolean isModified() {
		return modifiedProperty().getValue();
	}

	Property<Boolean> modifiedProperty();

}
