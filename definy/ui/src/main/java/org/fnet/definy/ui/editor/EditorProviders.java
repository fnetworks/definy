package org.fnet.definy.ui.editor;

import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class EditorProviders {

	private static InputStream resStream(String name) {
		return EditorProviders.class.getResourceAsStream(name);
	}

	@SuppressWarnings("HardCodedStringLiteral")
	public static void registerDefault() {
		EditorRegistry registry = EditorRegistry.getInstance(); // TODO glob associated with editor

		registry.registerEditorProvider(new DefaultCodeEditorProvider("Java", ".*\\.java", SyntaxConstants.SYNTAX_STYLE_JAVA));
		registry.registerEditorProvider(new DefaultCodeEditorProvider("C", ".*\\.[ch]", SyntaxConstants.SYNTAX_STYLE_C));
		registry.registerEditorProvider(new DefaultCodeEditorProvider("HTML", ".*\\.html?", SyntaxConstants.SYNTAX_STYLE_HTML));
		registry.registerEditorProvider(new DefaultCodeEditorProvider("JavaScript", ".*\\.js", SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT));
		registry.registerEditorProvider(new DefaultCodeEditorProvider("TypeScript", ".*\\.ts", SyntaxConstants.SYNTAX_STYLE_TYPESCRIPT));
		registry.registerEditorProvider(new DefaultCodeEditorProvider("JSON", ".*\\.json", SyntaxConstants.SYNTAX_STYLE_JSON));
		registry.registerEditorProvider(new DefaultCodeEditorProvider("CSS", ".*\\.css", SyntaxConstants.SYNTAX_STYLE_CSS));
		registry.registerEditorProvider(new DefaultCodeEditorProvider("C#", ".*\\.cs", SyntaxConstants.SYNTAX_STYLE_CSHARP));
		registry.registerEditorProvider(new DefaultCodeEditorProvider("Makefile", "Makefile|.*\\.make", SyntaxConstants.SYNTAX_STYLE_MAKEFILE));
		registry.registerEditorProvider(new DefaultCodeEditorProvider("YAML", ".*\\.ya?ml", SyntaxConstants.SYNTAX_STYLE_MAKEFILE));
		registry.registerEditorProvider(new DefaultCodeEditorProvider("XML", ".*\\.xml", SyntaxConstants.SYNTAX_STYLE_MAKEFILE));

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(resStream("/editors.txt"), StandardCharsets.UTF_8))) {
			String line;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.isEmpty())
					continue;
				try (InputStream in = resStream(String.format("/editors/%s.json", line))) {
					EditorRegistry.getInstance().registerEditorProvider(new LanguageContext(in));
				}
			}
		} catch (IOException e) {
			LoggerFactory.getLogger(EditorProviders.class).error("Error while reading editor index. Language support probably not available", e);
		}

		registry.setDefaultEditorProvider(new DefaultCodeEditorProvider("Plain", ".*", SyntaxConstants.SYNTAX_STYLE_NONE));
	}

}
