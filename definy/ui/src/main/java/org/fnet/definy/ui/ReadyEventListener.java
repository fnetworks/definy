package org.fnet.definy.ui;

import java.util.EventListener;

@FunctionalInterface
public interface ReadyEventListener extends EventListener {

	void onReady();

}
