package org.fnet.definy.ui.editor;

import org.fnet.definy.project.SourceFile;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EditorRegistry {

	private List<EditorProvider> providers = new ArrayList<>();

	private static EditorRegistry instance;
	private EditorProvider defaultProvider;

	public static EditorRegistry getInstance() {
		if (instance == null)
			instance = new EditorRegistry();
		return instance;
	}

	public void registerEditorProvider(EditorProvider provider) {
		if (provider == null)
			throw new NullPointerException("provider");
		if (providers.contains(provider))
			throw new RuntimeException("Provider already registered");
		providers.add(provider);
	}

	public void removeEditorProvider(EditorProvider provider) {
		providers.remove(provider);
	}

	public JComponent getEditorFor(SourceFile file) throws IOException {
		for (EditorProvider p : providers)
			if (p.canEdit(file.getFile()))
				return p.createEditor(file);
		return defaultProvider.createEditor(file);
	}

	public JComponent getEditorFor(File file) throws IOException {
		for (EditorProvider p : providers)
			if (p.canEdit(file))
				return p.createEditor(file);
		return defaultProvider.createEditor(file);
	}

	public void setDefaultEditorProvider(DefaultCodeEditorProvider provider) {
		this.defaultProvider = provider;
	}
}
