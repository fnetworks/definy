package org.fnet.definy.ui;

import com.alee.laf.text.WebTextPane;
import org.slf4j.LoggerFactory;

import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;

public class AppendableTextPane extends WebTextPane {

	public void appendColored(String text, Color color) {
		SimpleAttributeSet set = new SimpleAttributeSet();
		StyleConstants.setForeground(set, color);

		StyledDocument styledDocument = getStyledDocument();
		try {
			styledDocument.insertString(styledDocument.getLength(), text, set);
		} catch (BadLocationException e) {
			LoggerFactory.getLogger(AppendableTextPane.class).warn("Location error", e);
		}
	}

}
