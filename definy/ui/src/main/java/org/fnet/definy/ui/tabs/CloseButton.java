package org.fnet.definy.ui.tabs;

import com.alee.laf.button.WebButton;
import com.alee.managers.style.StyleId;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.InputStream;

public class CloseButton extends WebButton {

	private static ImageIcon closeIcon;
	private static ImageIcon closeIconHover;

	static {
		closeIcon = loadIcon("/icons/close_8.png");
		closeIconHover = loadIcon("/icons/close_hover_8.png");
	}

	private static ImageIcon loadIcon(String path) {
		try (InputStream res = EditorTabTitlePanel.class.getResourceAsStream(path)) {
			return new ImageIcon(ImageIO.read(res));
		} catch (IOException e) {
			LoggerFactory.getLogger(EditorTabTitlePanel.class).error("Could not load close icon", e);
		}
		return null;
	}

	public CloseButton() {
		super(StyleId.buttonIcon, closeIcon);
		setBorderPainted(false);
		setBorder(null);

		setMinimumSize(new Dimension(8, 8));

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				setIcon(closeIconHover);
				repaint();
			}

			@Override
			public void mouseExited(MouseEvent e) {
				setIcon(closeIcon);
				repaint();
			}
		});
	}
}
