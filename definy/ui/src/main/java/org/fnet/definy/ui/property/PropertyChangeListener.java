package org.fnet.definy.ui.property;

@FunctionalInterface
public interface PropertyChangeListener<T> {

	void onPropertyChanged(ModifyEvent<T> event);

}
