package org.fnet.definy.ui.tabs;

import com.alee.laf.tabbedpane.WebTabbedPane;
import org.fnet.definy.lang.Localizer;
import org.fnet.definy.project.SourceFile;
import org.fnet.definy.ui.AlertUtility;
import org.fnet.definy.ui.editor.EditorRegistry;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EditorTabPane {

	private WebTabbedPane pane;
	private List<Tab> tabs;

	public EditorTabPane() {
		this.pane = new WebTabbedPane();
		this.tabs = new ArrayList<>();
	}

	public void createTabFromFile(File file) throws IOException {
		for (Tab tab : tabs) {
			if (tab.getContent().getSource().equals(file)) {
				tab.focus();
				return;
			}
		}

		JComponent editorComponent = EditorRegistry.getInstance().getEditorFor(file);
		if (editorComponent == null) {
			AlertUtility.showAlert(pane, new RuntimeException("No editor found for " + file),
					Localizer.getString("editortabpane.error.openfile"));
			return; // TODO
		}

		EditorTabContentPanel contentPanel = new EditorTabContentPanel(file, editorComponent);
		EditorTabTitlePanel titlePanel = new EditorTabTitlePanel(this, contentPanel);
		Tab tab = new Tab(pane, file, titlePanel, contentPanel);
		tabs.add(tab);
		tab.addToPane(true);
	}

	public void createTabFromFile(SourceFile file) throws IOException {
		for (Tab tab : tabs) {
			if (tab.getContent().getSource().equals(file.getFile())) {
				tab.focus();
				return;
			}
		}

		JComponent editorComponent = EditorRegistry.getInstance().getEditorFor(file);
		if (editorComponent == null) {
			AlertUtility.showAlert(pane, new RuntimeException("No editor found for " + file),
					Localizer.getString("editortabpane.error.openfile"));
			return; // TODO
		}

		EditorTabContentPanel contentPanel = new EditorTabContentPanel(file.getFile(), editorComponent);
		EditorTabTitlePanel titlePanel = new EditorTabTitlePanel(this, contentPanel);
		Tab tab = new Tab(pane, file.getFile(), titlePanel, contentPanel);
		tabs.add(tab);
		tab.addToPane(true);
	}

	public void removeTab(Tab tab) {
		pane.removeTabAt(pane.indexOfComponent(tab.getContent()));
		tabs.remove(tab);
	}

	void removeTab(EditorTabContentPanel content) {
		for (Tab tab : tabs) {
			if (tab.getContent().equals(content)) {
				removeTab(tab);
				return;
			}
		}
	}

	public void removeTab(File file) {
		for (Tab tab : tabs) {
			if (tab.getContent().getSource().equals(file)) {
				removeTab(tab);
				return;
			}
		}
	}

	public WebTabbedPane getComponent() {
		return pane;
	}

	public List<Tab> getTabs() {
		return tabs;
	}
}
