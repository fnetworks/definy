package org.fnet.definy.ui;

import com.alee.extended.filechooser.WebDirectoryChooser;
import com.alee.laf.menu.WebMenu;
import com.alee.laf.menu.WebMenuBar;
import com.alee.laf.menu.WebMenuItem;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.window.WebFrame;
import org.fnet.definy.lang.Localizer;
import org.fnet.definy.project.Project;
import org.fnet.definy.settings.AppSettings;
import org.fnet.definy.util.CommonDirectories;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.EventListenerList;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

public class MainFrame extends WebFrame {

	private EventListenerList eventListeners = new EventListenerList();

	private final WebMenuBar menuBar;
	private final WebPanel statusBar;

	private MainContentPane contentPane;

	private Project currentProject;

	public MainFrame() {
		this.setTitle(Localizer.instance().get("mainframe.title"));
		this.setSize(new Dimension(1280, 720));
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());

		menuBar = new WebMenuBar();
		this.setJMenuBar(menuBar);

		buildMenuBar();

		statusBar = new WebPanel();
		statusBar.setBorder(new MatteBorder(1, 0, 0, 0, Color.GRAY));
		statusBar.setPadding(5);
		add(statusBar, BorderLayout.PAGE_END);

		statusBar.add(new JLabel("Status"));

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO check for unsaved files
				if (contentPane != null) {
					try {
						contentPane.writeEditorStateToProject();
					} catch (IOException e1) {
						LoggerFactory.getLogger(MainFrame.class).error("Could not save editor state", e1);
					}
				}
				dispose();
				for (CloseEventListener listener : eventListeners.getListeners(CloseEventListener.class))
					listener.onClose();
			}
		});
	}

	private JMenu getOrCreateTopLevelMenu(String name) {
		for (int i = 0; i < menuBar.getMenuCount(); i++) {
			JMenu menu = menuBar.getMenu(i);
			if (menu.getName().equals(name))
				return menu;
		}
		JMenu newMenu = new WebMenu(name);
		menuBar.add(newMenu);
		return newMenu;
	}

	public void openProject(File projectDirectory) throws IOException {
		this.currentProject = new Project(projectDirectory);
		currentProject.load();
		MainContentPane contentPane = new MainContentPane(currentProject);
		setContentPane(contentPane);
		contentPane.restoreEditorStateFromProject();
		AppSettings.getAppSettings().getContainer().setLastProjectLocation(projectDirectory);
	}

	public void setProject(Project project) throws IOException {
		if (project == null)
			throw new NullPointerException("project");
		this.currentProject = project;
		MainContentPane contentPane = new MainContentPane(currentProject);
		setContentPane(contentPane);
		contentPane.restoreEditorStateFromProject();
		AppSettings.getAppSettings().getContainer().setLastProjectLocation(project.getLocation());
	}

	private void buildMenuBar() {
		JMenu file = getOrCreateTopLevelMenu(Localizer.getString("mainframe.menu.file"));
		menuBar.add(file);

		JMenuItem openProjectMenuItem = new WebMenuItem(Localizer.getString("mainframe.menu.file.openproject"));
		openProjectMenuItem.addActionListener(e -> {
			File open = WebDirectoryChooser.showDialog(MainFrame.this, CommonDirectories.getCurrentDirectory());
			if (open == null)
				return;
			try {
				openProject(open);
				revalidate();
				repaint();
			} catch (Exception e1) {
				AlertUtility.showAlert(this, e1, Localizer.getString("mainframe.openproject.error"));
				LoggerFactory.getLogger(getClass()).error("Failed to open project", e1);
			}
		});
		file.add(openProjectMenuItem);

		JMenuItem newProjectMenuItem = new WebMenuItem(Localizer.getString("mainframe.menu.file.createproject"));
		newProjectMenuItem.addActionListener(e -> {
			File open = WebDirectoryChooser.showDialog(MainFrame.this, CommonDirectories.getCurrentDirectory());
			if (open == null)
				return;
			Project project = new Project(open);
			try {
				project.save();
				setProject(project);
			} catch (IOException e1) {
				AlertUtility.showAlert(this, e1, Localizer.getString("mainframe.createproject.error"));
				LoggerFactory.getLogger(getClass()).error("Failed to create project", e1);
			}
		});
		file.add(newProjectMenuItem);

		JMenuItem projectSettingsMenuItem = new WebMenuItem("Project settings");
		projectSettingsMenuItem.addActionListener(e -> new ProjectSettingsDialog(currentProject).setVisible(true));
		file.add(projectSettingsMenuItem);
	}

	private void setContentPane(MainContentPane contentPane) {
		if (this.contentPane != null)
			this.remove(this.contentPane);
		if (contentPane != null) {
			this.contentPane = contentPane;
			this.add(contentPane, BorderLayout.CENTER);
			revalidate();
			repaint();
		}
	}

	public void addCloseListener(CloseEventListener listener) {
		eventListeners.add(CloseEventListener.class, listener);
	}

	public void removeCloseListener(CloseEventListener listener) {
		eventListeners.remove(CloseEventListener.class, listener);
	}

}
