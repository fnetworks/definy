package org.fnet.definy.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageView extends JPanel {
	private BufferedImage image;
	private boolean preserveRatio = true;

	public ImageView(BufferedImage image) {
		this.image = image;
	}

	public ImageView(BufferedImage image, boolean preserveRatio) {
		this.image = image;
		this.preserveRatio = preserveRatio;
	}

	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public boolean isPreserveRatio() {
		return preserveRatio;
	}

	public void setPreserveRatio(boolean preserveRatio) {
		this.preserveRatio = preserveRatio;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		if (preserveRatio) {
			g2d.drawImage(image, 0, 0, null);
		} else {
			g2d.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		}
	}

}
