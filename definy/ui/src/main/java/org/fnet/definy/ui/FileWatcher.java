package org.fnet.definy.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FileWatcher {

	@FunctionalInterface
	public interface FileChangeListener {
		void onFileChanged(File file);
	}

	private final WatchService watcher;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private Thread fileListenerThread;
	private File directory;
	private List<FileChangeListener> fileChangeListeners;

	private Map<WatchKey, Path> keys = new HashMap<>();

	public FileWatcher(File directory) throws IOException {
		this.directory = directory;
		this.fileListenerThread = new Thread(this::run);
		fileListenerThread.setDaemon(true);
		this.fileChangeListeners = new LinkedList<>();
		watcher = FileSystems.getDefault().newWatchService();
	}

	private void run() {
		try {
			registerWatchDirectory(directory.toPath());

			WatchKey key;
			while ((key = watcher.take()) != null) {
				try {
					for (WatchEvent<?> pollEvent : key.pollEvents()) {
						WatchEvent.Kind<?> kind = pollEvent.kind();
						if (kind == StandardWatchEventKinds.OVERFLOW)
							continue;
						@SuppressWarnings("unchecked")
						WatchEvent<Path> watchEvent = (WatchEvent<Path>) pollEvent;
						Path filename = watchEvent.context();
						Path absPath = keys.get(key).resolve(filename);
						if (Files.isDirectory(absPath)) {
							if (kind == StandardWatchEventKinds.ENTRY_CREATE)
								registerWatchDirectory(absPath);
							else if (kind == StandardWatchEventKinds.ENTRY_DELETE)
								removeWatchDirectory(absPath);
						}
						File file = absPath.toFile();
						logger.trace("File {} changed on disk: {}", file, kind);
						fileChangeListeners.forEach(e -> e.onFileChanged(file));
					}
					if (key.isValid() && !key.reset()) {
						logger.warn("Error while resetting watch key: {}", keys.get(key));
					}
				} catch (IOException e) {
					logger.error("Error while processing watch key", e);
				}
			}
		} catch (IOException e) {
			logger.error("Error while watching for file changes", e);
		} catch (InterruptedException e) {
			logger.warn("File watcher thread interrupted");
		}
	}

	private void registerWatchDirectory(Path root) throws IOException {
		Files.walkFileTree(root, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				WatchKey key = dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);
				keys.put(key, dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	private void removeWatchDirectory(Path path) {
		keys.entrySet().stream()
				.filter(e -> e.getValue().equals(path))
				.findFirst()
				.map(Map.Entry::getKey)
				.ifPresent(key -> {
					key.cancel();
					keys.remove(key);
				});
	}

	public void start() {
		fileListenerThread.start();
	}

	public void interrupt() {
		fileListenerThread.interrupt();
	}

	public void addFileChangeListener(FileChangeListener listener) {
		if (listener == null)
			return;
		fileChangeListeners.add(listener);
	}

}
