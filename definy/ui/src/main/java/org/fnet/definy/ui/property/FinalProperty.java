package org.fnet.definy.ui.property;

public class FinalProperty<T> implements Property<T> {

	private T value;

	public FinalProperty(T initialValue) {
		this.value = initialValue;
	}

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public void setValue(T value) {
		throw new UnsupportedOperationException("setValue is not supported on FinalProperty");
	}

	@Override
	public void addPropertyChangeListener(PropertyChangeListener<T> changeListener) {
	}

	@Override
	public boolean removePropertyChangeListener(PropertyChangeListener<T> listener) {
		return false;
	}
}
