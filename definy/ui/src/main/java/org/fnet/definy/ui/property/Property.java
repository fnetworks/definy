package org.fnet.definy.ui.property;

public interface Property<T> {
	T getValue();

	void setValue(T value);

	void addPropertyChangeListener(PropertyChangeListener<T> changeListener);

	boolean removePropertyChangeListener(PropertyChangeListener<T> listener);
}
