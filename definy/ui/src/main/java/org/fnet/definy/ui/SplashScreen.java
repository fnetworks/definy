package org.fnet.definy.ui;

import org.jetbrains.annotations.NonNls;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SplashScreen extends JDialog {

	@NonNls
	private static final String SPLASH_IMAGE_LOCATION = "/splash.png";

	private ImageView imageView;

	private List<ReadyEventListener> readyEventListeners = new ArrayList<>();

	public SplashScreen() {
		setLayout(new BorderLayout());
		setBackground(new Color(29, 131, 216));

		BufferedImage splashImage;
		try {
			splashImage = ImageIO.read(getClass().getResourceAsStream(SPLASH_IMAGE_LOCATION));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		this.imageView = new ImageView(splashImage);
		imageView.setPreserveRatio(false);
		imageView.setPreferredSize(new Dimension(splashImage.getWidth(), splashImage.getHeight()));
		add(imageView, BorderLayout.CENTER);

		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		progressBar.setBorder(null);
		progressBar.setBorderPainted(false);
		add(progressBar, BorderLayout.SOUTH);

		setUndecorated(true);
		pack();
		setLocationRelativeTo(null);

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {
				readyEventListeners.forEach(ReadyEventListener::onReady);
			}
		});
	}

	public void addReadyEventListener(ReadyEventListener eventListener) {
		if (eventListener == null)
			return;
		readyEventListeners.add(eventListener);
	}

}
