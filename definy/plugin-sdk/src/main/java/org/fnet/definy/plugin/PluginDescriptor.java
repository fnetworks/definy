package org.fnet.definy.plugin;

import org.fnet.definy.data.DataObject;
import org.fnet.definy.data.Loadable;

public class PluginDescriptor implements Loadable {

	private String id;
	private String name;
	private String vendor;
	private String description;
	private String mainClass;

	PluginDescriptor() {
	}

	public PluginDescriptor(String id, String name, String vendor, String description, String mainClass) {
		this.id = id;
		this.name = name;
		this.vendor = vendor;
		this.description = description;
		this.mainClass = mainClass;
	}

	@Override
	public void load(DataObject data) {
		this.id = data.getString("id");
		this.name = data.getString("name", id);
		this.vendor = data.getString("vendor", null);
		this.description = data.getString("description", null);
		this.mainClass = data.getString("mainClass");
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getVendor() {
		return vendor;
	}

	public String getDescription() {
		return description;
	}

	public String getMainClass() {
		return mainClass;
	}
}
