package org.fnet.definy.plugin;

import org.fnet.definy.data.DataSerializers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class PluginLoader {

	public PluginInstance loadPluginFromJar(File jarFile) throws IOException, ReflectiveOperationException {
		if (!jarFile.exists())
			throw new FileNotFoundException(jarFile.getName());
		PluginClassLoader loader = new PluginClassLoader(jarFile, getClass().getClassLoader());
		PluginDescriptor descriptor = new PluginDescriptor();
		try (InputStream in = Objects.requireNonNull(loader.getResourceAsStream("plugin.json"))) {
			descriptor.load(DataSerializers.deserialize("json", in));
		}
		IPlugin instance = Class
				.forName(descriptor.getMainClass(), true, loader)
				.asSubclass(IPlugin.class)
				.getConstructor()
				.newInstance();
		return new PluginInstance(instance, loader, descriptor);
	}

}
