package org.fnet.definy.plugin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PluginSystem {

	private File directory;
	private PluginLoader loader;
	private List<PluginInstance> loadedPlugins;

	private Logger logger = LoggerFactory.getLogger(PluginSystem.class);

	public PluginSystem(File directory) {
		this.directory = directory;
		this.loader = new PluginLoader();
		this.loadedPlugins = new ArrayList<>();
	}

	/**
	 * Loads all plugins and sets their state to {@link PluginState#LOADED}
	 */
	public void loadAll() throws IOException, ReflectiveOperationException {
		if (!directory.exists()) {
			logger.warn("Plugin directory doesn't exist, no plugins loaded");
			return;
		}
		File[] files = directory.listFiles(f -> f.getName().endsWith(".jar"));
		if (files == null) {
			logger.warn("Could not list plugins, no plugins loaded");
			return;
		}
		for (File f : files)
			loadedPlugins.add(loader.loadPluginFromJar(f));
		setAllStates(PluginState.LOADED);
		logger.info("Loaded {} plugins", loadedPlugins.size());
	}

	/**
	 * Sets the state for all plugins
	 *
	 * @param state The state to set
	 */
	public void setAllStates(PluginState state) {
		for (PluginInstance plugin : loadedPlugins) {
			plugin.setState(state);
		}
	}

	public void shutdown() {
		setAllStates(PluginState.UNLOADED);
		for (PluginInstance instance : loadedPlugins) {
			try {
				instance.getClassLoader().close();
			} catch (IOException e) {
				logger.error("Failed to close plugin class loader", e);
			}
		}
	}


}
