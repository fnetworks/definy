package org.fnet.definy.plugin;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class PluginClassLoader extends URLClassLoader {

	public PluginClassLoader(URL url, ClassLoader parent) {
		super(new URL[]{url}, parent);
	}

	public PluginClassLoader(File file, ClassLoader parent) throws MalformedURLException {
		this(file.toURI().toURL(), parent);
	}

}
