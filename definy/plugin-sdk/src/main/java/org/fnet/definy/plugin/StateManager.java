package org.fnet.definy.plugin;

import java.util.HashMap;
import java.util.Map;

public class StateManager {

	@FunctionalInterface
	public interface TransferAction {
		void transferState(IPlugin instance) throws Exception;
	}

	private Map<PluginState, Map<PluginState, TransferAction>> stateTransferMap = new HashMap<>();

	public StateManager() {
		defineTransferAction(PluginState.UNLOADED, PluginState.LOADED, IPlugin::onLoad);
		defineTransferAction(PluginState.LOADED, PluginState.ACTIVE, IPlugin::onActivate);
		defineTransferAction(PluginState.ACTIVE, PluginState.LOADED, IPlugin::onDeactivate);
		defineTransferAction(PluginState.LOADED, PluginState.UNLOADED, IPlugin::onUnload);
	}

	private void defineTransferAction(PluginState from, PluginState to, TransferAction action) {
		getTargetActions(from).put(to, action);
	}

	private Map<PluginState, TransferAction> getTargetActions(PluginState currentState) {
		return stateTransferMap.computeIfAbsent(currentState, e -> new HashMap<>());
	}

	public void setStateOf(PluginInstance instance, PluginState newState) throws IllegalStateException {
		PluginState currentState = instance.getState();
		if (currentState == newState)
			return;
		TransferAction action = getTargetActions(currentState).get(newState);
		if (action == null)
			throw new IllegalStateException(String.format("%s -> %s", currentState, newState));
		try {
			action.transferState(instance.getPlugin());
		} catch (Exception e) {
			throw new RuntimeException("Plugin errored on state transfer", e);
		}
	}

}
