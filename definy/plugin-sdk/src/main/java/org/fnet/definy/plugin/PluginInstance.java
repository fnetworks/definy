package org.fnet.definy.plugin;

public class PluginInstance {

	private static final StateManager STATE_MANAGER = new StateManager();

	private IPlugin plugin;
	private PluginClassLoader classLoader;
	private PluginDescriptor descriptor;
	private PluginState state;

	public PluginInstance(IPlugin plugin, PluginClassLoader classLoader, PluginDescriptor descriptor) {
		this.plugin = plugin;
		this.classLoader = classLoader;
		this.descriptor = descriptor;
		this.state = PluginState.UNLOADED;
	}

	public IPlugin getPlugin() {
		return plugin;
	}

	public PluginClassLoader getClassLoader() {
		return classLoader;
	}

	public PluginDescriptor getDescriptor() {
		return descriptor;
	}

	public void setState(PluginState newState) {
		STATE_MANAGER.setStateOf(this, newState);
		this.state = newState;
	}

	public PluginState getState() {
		return state;
	}
}
