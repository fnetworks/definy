package org.fnet.definy.plugin;

public interface IPlugin {

	void onLoad() throws Exception;

	void onUnload();

	default void onActivate() {
	}

	default void onDeactivate() {
	}

}
