package org.fnet.definy.plugin;

/**
 * Represents the lifecycle state of a plugin.
 * <br>
 * UNLOADED -> LOADED -> ACTIVE -> LOADED -> UNLOADED
 */
public enum PluginState {

	UNLOADED, LOADED, ACTIVE

}
