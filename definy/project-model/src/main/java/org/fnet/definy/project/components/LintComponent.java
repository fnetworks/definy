package org.fnet.definy.project.components;

import org.fnet.definy.project.Module;
import org.fnet.definy.project.compiler.CompilerMessage;
import org.fnet.definy.project.compiler.ILinter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LintComponent extends Component {

	private ILinter linter;

	public LintComponent(Module module, ILinter linter) {
		super(module);
		this.linter = linter;
	}

	public List<CompilerMessage> lint(File file) throws IOException, InterruptedException {
		List<CompilerMessage> messages = new ArrayList<>();
		linter.lint(file, getModule(), messages);
		return messages;
	}

	public ILinter getLinter() {
		return linter;
	}
}
