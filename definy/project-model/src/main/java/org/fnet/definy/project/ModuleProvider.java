package org.fnet.definy.project;

import java.io.File;

public interface ModuleProvider {

	String getName();

	Module provideModule(Project project, File location);

}
