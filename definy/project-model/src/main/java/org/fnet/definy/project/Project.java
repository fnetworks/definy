package org.fnet.definy.project;

import org.fnet.definy.data.DataObject;
import org.fnet.definy.data.DataSerializers;
import org.fnet.definy.util.FileUtil;
import org.fnet.definy.util.streamprinter.PrintTarget;
import org.jetbrains.annotations.NonNls;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@ClassConverterHint(".name")
public class Project {

	@NonNls
	private static final String PROJECT_FILE_NAME = "definy.json";

	@Property
	private String name;
	@Property
	private File location, definitionFile;

	@Property
	private File metadataDirectory;

	@Property
	private Module[] modules;
	@Property(special = "&modules")
	private Module mainModule;

	public Project(File location) {
		if (location.getName().equals(PROJECT_FILE_NAME))
			location = location.getParentFile();
		this.location = location;
		this.definitionFile = new File(location, PROJECT_FILE_NAME);
		this.metadataDirectory = new File(location, ".definy");
	}

	public void load() throws IOException {
		DataObject data = DataSerializers.deserialize(definitionFile);
		this.name = data.getString("name");

		DataObject[] moduleDatas = data.getObjectArray("modules");
		List<Module> modules = new ArrayList<>(moduleDatas.length);
		for (DataObject moduleData : moduleDatas) {
			String type = moduleData.getString("type");
			ModuleProvider provider = ModuleFactory.getInstance().getProvider(type);
			if (provider == null)
				throw new RuntimeException("Unknown module type");
			Module module = provider.provideModule(this, new File(location, moduleData.getString("name")));

			module.load(moduleData);
			module.createComponents();
			modules.add(module);
		}
		this.modules = modules.toArray(new Module[0]);

		if (data.has("main")) {
			String mainName = data.getString("main");
			for (Module m : modules) {
				if (m.getName().equals(mainName)) {
					this.mainModule = m;
					break;
				}
			}
		}
	}

	public void save() throws IOException {
		DataObject root = new DataObject();
		root.setString("name", name);
		if (mainModule != null)
			root.setString("main", mainModule.getName());
		List<DataObject> moduleDatas = new ArrayList<>(modules.length);
		for (Module module : modules) {
			DataObject moduleData = new DataObject();
			module.save(moduleData);
			moduleDatas.add(moduleData);
		}
		root.setObjectArray("modules", moduleDatas.toArray(new DataObject[0]));

		FileUtil.mkdirs(location);
		DataSerializers.serialize(root, definitionFile);
	}

	public void build(PrintTarget outputTarget) throws Exception {
		Graph<Module> moduleGraph = new Graph<>();
		for (Module mod : modules) {
			for (String dep : mod.getDependencies()) {
				moduleGraph.addEdge(mod, getModuleByName(dep));
			}
		}

		Module[] sortedModules = moduleGraph.topologicalSort(mainModule).toArray(new Module[0]);
		for (int i = sortedModules.length - 1; i >= 0; i--) {
			Module m = sortedModules[i];
			m.build(outputTarget);
		}
	}

	public void run(PrintTarget outputTarget) throws Exception {
		if (mainModule != null) {
			build(outputTarget);
			mainModule.run(outputTarget);
		}
	}

	public File getLocation() {
		return location;
	}

	public File getDefinitionFile() {
		return definitionFile;
	}

	public File getMetadataDirectory() {
		return metadataDirectory;
	}

	public String getName() {
		return name;
	}

	public Module[] getModules() {
		return modules;
	}

	public Module getMainModule() {
		return mainModule;
	}

	public Module getModuleByName(String name) {
		for (Module m : modules)
			if (m.getName().equalsIgnoreCase(name))
				return m;
		return null;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLocation(File location) {
		this.location = location;
	}

	public void setDefinitionFile(File definitionFile) {
		this.definitionFile = definitionFile;
	}

	public void setModules(Module[] modules) {
		this.modules = modules;
	}

	public void setMainModule(Module mainModule) {
		this.mainModule = mainModule;
	}
}
