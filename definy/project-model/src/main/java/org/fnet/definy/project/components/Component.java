package org.fnet.definy.project.components;

import org.fnet.definy.project.Module;

public abstract class Component {

	private Module module;
	private Class<? extends Component>[] dependencies;

	public Component(Module module, Class<? extends Component>... dependencies) {
		this.module = module;
		this.dependencies = dependencies;
	}

	public Module getModule() {
		return module;
	}

	public Class<? extends Component>[] getDependencies() {
		return dependencies;
	}
}
