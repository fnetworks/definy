package org.fnet.definy.project;

import java.io.IOException;
import java.util.List;

public interface DependencyScanner {

	void scanDependencies(SourceFile sourceFile, List<SourceFile> dependencies) throws IOException;

}
