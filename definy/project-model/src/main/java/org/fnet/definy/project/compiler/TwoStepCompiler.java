package org.fnet.definy.project.compiler;

import org.fnet.definy.project.Module;
import org.fnet.definy.util.FileUtil;
import org.fnet.definy.util.streamprinter.PrintTarget;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Compiler with a typical compile-link cycle
 */
public abstract class TwoStepCompiler implements ICompiler {

	@Override
	public final void compile(File[] sources, File output, Module module, PrintTarget printTarget, List<CompilerMessage> compilerMessages) throws Exception {
		if (sources.length == 0)
			throw new Exception("Missing input files");
		List<File> compiledFiles = new ArrayList<>(sources.length);
		boolean recompile = false;
		for (File input : sources) {
			File outputFile = FileUtil.relativizeWithExt(input, module.getSourceDirectory(),
					module.getIntermediateBinariesDirectory(), ".o");
			compiledFiles.add(outputFile);
			if (outputFile.exists() && outputFile.lastModified() > input.lastModified())
				continue;
			recompile = true;
			FileUtil.mkdirs(outputFile.getParentFile());
			compile(input, outputFile, module, printTarget, compilerMessages);
		}

		if (recompile || !output.exists()) {
			FileUtil.mkdirs(output.getParentFile());
			link(compiledFiles.toArray(new File[0]), output, module, printTarget, compilerMessages);
		}
	}

	protected abstract void compile(File source, File output, Module module, PrintTarget printTarget, List<CompilerMessage> compilerMessages) throws Exception;

	protected abstract void link(File[] inputs, File output, Module module, PrintTarget printTarget, List<CompilerMessage> compilerMessages) throws Exception;

}
