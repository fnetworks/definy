package org.fnet.definy.project;

import java.util.ArrayList;
import java.util.List;

public class ModuleFactory {

	private static ModuleFactory instance;

	public static ModuleFactory getInstance() {
		if (instance == null)
			instance = new ModuleFactory();
		return instance;
	}

	private List<ModuleProvider> providers = new ArrayList<>();

	private ModuleFactory() {
	}

	public ModuleProvider getProvider(String name) {
		for (ModuleProvider p : providers)
			if (p.getName().equalsIgnoreCase(name))
				return p;
		return null;
	}

	public void addProvider(ModuleProvider provider) {
		this.providers.add(provider);
	}

	public void removeProvider(ModuleProvider provider) {
		this.providers.remove(provider);
	}

	public List<ModuleProvider> getProviders() {
		return providers;
	}
}
