package org.fnet.definy.project.compiler;

import org.fnet.definy.project.Module;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface ILinter {

	void lint(File input, Module module, List<CompilerMessage> compilerMessages) throws IOException, InterruptedException;

}
