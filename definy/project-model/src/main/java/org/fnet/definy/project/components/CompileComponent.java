package org.fnet.definy.project.components;

import org.fnet.definy.project.Module;
import org.fnet.definy.project.compiler.CompilerMessage;
import org.fnet.definy.project.compiler.ICompiler;
import org.fnet.definy.util.streamprinter.PrintTarget;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CompileComponent extends Component {

	private ICompiler compiler;

	public CompileComponent(Module module, ICompiler compiler) {
		super(module);
		this.compiler = compiler;
	}

	public List<CompilerMessage> compile(File[] files, File output, PrintTarget printTarget) throws Exception {
		List<CompilerMessage> messages = new ArrayList<>();
		compiler.compile(files, output, getModule(), printTarget, messages);
		return messages;
	}

	public ICompiler getCompiler() {
		return compiler;
	}
}
