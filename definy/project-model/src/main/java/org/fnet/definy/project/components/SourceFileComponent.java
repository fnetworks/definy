package org.fnet.definy.project.components;

import org.fnet.definy.project.Module;
import org.fnet.definy.project.SourceFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class SourceFileComponent extends Component {

	private final FileFilter fileFilter;
	private final Consumer<SourceFile> configurator;
	private final File[] sourceDirectories;
	private final Logger logger = LoggerFactory.getLogger(SourceFileComponent.class);

	private SourceFile[] sourceFiles;

	public SourceFileComponent(Module module, FileFilter fileFilter, Consumer<SourceFile> configurator, File... sourceDirectories) {
		super(module);
		this.fileFilter = fileFilter;
		this.configurator = configurator;
		this.sourceDirectories = sourceDirectories;
	}

	public void scanSourceFiles() {
		this.sourceFiles = Arrays.stream(sourceDirectories)
				.filter(File::exists)
				.flatMap(file -> {
					try {
						return Files.walk(file.toPath());
					} catch (IOException e) {
						logger.error("Could not walk directory", e);
						return Stream.empty();
					}
				})
				.filter(path -> !Files.isDirectory(path))
				.map(path -> {
					SourceFile sourceFile = new SourceFile(path.toFile(), getModule());
					configurator.accept(sourceFile);
					return sourceFile;
				}).toArray(SourceFile[]::new);
	}

	public SourceFile[] getSourceFiles() {
		if (sourceFiles == null)
			scanSourceFiles();
		return sourceFiles;
	}

	public FileFilter getFileFilter() {
		return fileFilter;
	}

	public Consumer<SourceFile> getConfigurator() {
		return configurator;
	}

	public File[] getSourceDirectories() {
		return sourceDirectories;
	}

	public SourceFile getSourceFile(String path) {
		for (File sourceDirectory : getSourceDirectories()) {
			File file = new File(sourceDirectory, path);
			if (file.exists())
				return getSourceFile(file);
		}
		return null;
	}

	public SourceFile getSourceFile(File file) {
		for (SourceFile s : getSourceFiles())
			if (s.getFile().equals(file))
				return s;
		return null;
	}

}
