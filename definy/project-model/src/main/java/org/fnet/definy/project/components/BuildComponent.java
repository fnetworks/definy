package org.fnet.definy.project.components;

import org.fnet.definy.project.Module;
import org.fnet.definy.project.SourceFile;
import org.fnet.definy.project.compiler.CompilerMessage;
import org.fnet.definy.util.streamprinter.PrintTarget;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BuildComponent extends Component {

	public BuildComponent(Module module) {
		super(module, CompileComponent.class, SourceFileComponent.class);
	}

	public List<CompilerMessage> build(PrintTarget outputTarget) throws Exception {
		List<File> inputFiles = new ArrayList<>();
		for (SourceFile sourceFile : getModule().getComponent(SourceFileComponent.class).getSourceFiles()) {
			if (!sourceFile.getMetadata().getBoolean("isHeader"))
				inputFiles.add(sourceFile.getFile());
		}
		File finalOutput = new File(getModule().getBinariesDirectory(), getModule().getNormalizedName());
		return getModule().getComponent(CompileComponent.class)
				.compile(inputFiles.toArray(new File[0]), finalOutput, outputTarget);
	}

}
