package org.fnet.definy.project;

import java.util.*;

public class Graph<T> {

	private static class Edge<T> {
		private T start, end;

		Edge(T start, T end) {
			this.start = start;
			this.end = end;
		}
	}

	private List<Edge<T>> edges = new ArrayList<>();

	public void addEdge(T start, T end) {
		edges.add(new Edge<>(start, end));
	}

	private List<T> getNeighbors(T t) {
		List<T> neighbors = new ArrayList<>();
		for (Edge<T> e : edges)
			if (e.start.equals(t))
				neighbors.add(e.end);
		return neighbors;
	}

	public Collection<T> topologicalSort(T root) {
		Deque<T> stack = new LinkedList<>();
		topologicalSort(root, stack, new LinkedList<>());
		return stack;
	}

	private void topologicalSort(T node, Deque<T> stack, List<T> visited) {
		List<T> neighbors = getNeighbors(node);
		for (T t : neighbors) {
			if (t != null && !visited.contains(t)) {
				topologicalSort(t, stack, visited);
				visited.add(t);
			}
		}
		stack.push(node);
	}

}
