package org.fnet.definy.project;

import org.fnet.definy.project.components.SourceFileComponent;
import org.jetbrains.annotations.NonNls;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class CDependencyScanner implements DependencyScanner { // TODO move to plugin

	@NonNls
	private static final String INCLUDE_START = "#include ";

	@Override
	public void scanDependencies(SourceFile sourceFile, List<SourceFile> dependencies) throws IOException {
		Files.readAllLines(sourceFile.getFile().toPath()).forEach(line -> {
			line = line.trim();
			if (line.startsWith(INCLUDE_START)) {
				line = line.substring(INCLUDE_START.length());
				if (line.startsWith("\"") && line.endsWith("\"") || line.startsWith("<") && line.endsWith(">")) {
					line = line.substring(1, line.length() - 1);
					SourceFile dep = sourceFile.getModule().getComponent(SourceFileComponent.class).getSourceFile(line);
					if (dep == null)
						dep = sourceFile.getModule().getComponent(SourceFileComponent.class)
								.getSourceFile(new File(sourceFile.getFile().getParentFile(), line));
					if (dep != null)
						dependencies.add(dep); // TODO external dependencies
				}
			}
		});
	}


}
