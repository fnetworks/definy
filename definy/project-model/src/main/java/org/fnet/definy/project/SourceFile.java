package org.fnet.definy.project;

import org.fnet.definy.data.DataObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SourceFile {

	private File file;
	private Module module;
	private List<SourceFile> dependencies = new ArrayList<>();
	private DependencyScanner dependencyScanner = new CDependencyScanner();
	private DataObject metadata = new DataObject();

	public SourceFile(File file, Module module) {
		this.file = file;
		this.module = module;
	}

	public File getFile() {
		return file;
	}

	public Module getModule() {
		return module;
	}

	public List<SourceFile> getDependencies() {
		return dependencies;
	}

	public void scanDependencies() throws IOException {
		dependencies.clear();
		dependencyScanner.scanDependencies(this, dependencies);
	}

	public DataObject getMetadata() {
		return metadata;
	}
}
