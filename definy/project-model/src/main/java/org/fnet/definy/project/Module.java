package org.fnet.definy.project;

import org.fnet.definy.data.DataObject;
import org.fnet.definy.data.Persistable;
import org.fnet.definy.project.components.Component;
import org.fnet.definy.util.streamprinter.PrintTarget;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public abstract class Module implements Persistable {

	private List<Component> components = new ArrayList<>();

	@Property
	private String name;
	@Property
	private List<String> dependencies = new ArrayList<>();

	@Property
	private File location;
	@Property
	private File sourceDirectory, includeDirectory, binariesDirectory, intermediateBinariesDirectory;

	public Module(File location) {
		this.location = location;
	}

	@Override
	public void save(DataObject data) {
		data.setString("name", this.name);
		if (location != null)
			data.setString("location", location.getPath());
		if (dependencies != null && dependencies.size() > 0)
			data.setStringArray("dependencies", dependencies.toArray(new String[0]));
	}

	@Override
	public void load(DataObject data) {
		this.name = data.getString("name");
		if (data.has("dependencies"))
			this.dependencies = Arrays.asList(data.getStringArray("dependencies"));
		if (data.has("location"))
			this.location = new File(data.getString("location", name));
	}

	public void createComponents() {
	}

	public abstract void build(PrintTarget outputTarget) throws Exception;

	public abstract void run(PrintTarget outputTarget) throws Exception;

	public void loadDefaults() {
//		this.sourceDirectory = new File(location, "src");
//		this.includeDirectory = new File(location, "include");
//		this.binariesDirectory = new File(location, "bin");
//		this.intermediateBinariesDirectory = new File(binariesDirectory, "intermediate");
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Module)) return false;
		Module module = (Module) o;
		return getName().equals(module.getName()) &&
				Objects.equals(dependencies, module.dependencies) &&
				Objects.equals(getLocation(), module.getLocation()) &&
				Objects.equals(getSourceDirectory(), module.getSourceDirectory()) &&
				Objects.equals(getIncludeDirectory(), module.getIncludeDirectory()) &&
				Objects.equals(getBinariesDirectory(), module.getBinariesDirectory()) &&
				Objects.equals(getIntermediateBinariesDirectory(), module.getIntermediateBinariesDirectory());
	}

	@Override
	public int hashCode() {
		int result = Objects.hash(getName(), getLocation(), getSourceDirectory(), getIncludeDirectory(), getBinariesDirectory(), getIntermediateBinariesDirectory(), getDependencies());
		result = 31 * result;
		return result;
	}

	public String getName() {
		return name;
	}

	public String getNormalizedName() {
		return getName().replaceAll("[^a-zA-Z0-9_\\-]", "-");
	}

	@Override
	public String toString() {
		return String.format("%s [%s]", getName(), getClass().getSimpleName());
	}

	public File getLocation() {
		return location;
	}

	public File getSourceDirectory() {
		if (sourceDirectory == null)
			sourceDirectory = new File(location, "src");
		return sourceDirectory;
	}

	public File getIncludeDirectory() {
		if (includeDirectory == null)
			includeDirectory = new File(location, "include");
		return includeDirectory;
	}

	public File getBinariesDirectory() {
		if (binariesDirectory == null)
			binariesDirectory = new File(location, "bin");
		return binariesDirectory;
	}

	public File getIntermediateBinariesDirectory() {
		if (intermediateBinariesDirectory == null)
			intermediateBinariesDirectory = new File(getBinariesDirectory(), "obj");
		return intermediateBinariesDirectory;
	}

	public List<String> getDependencies() {
		return dependencies;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLocation(File location) {
		this.location = location;
	}

	public void setSourceDirectory(File sourceDirectory) {
		this.sourceDirectory = sourceDirectory;
	}

	public void setIncludeDirectory(File includeDirectory) {
		this.includeDirectory = includeDirectory;
	}

	public void setBinariesDirectory(File binariesDirectory) {
		this.binariesDirectory = binariesDirectory;
	}

	public void setIntermediateBinariesDirectory(File intermediateBinariesDirectory) {
		this.intermediateBinariesDirectory = intermediateBinariesDirectory;
	}

	public List<Component> getComponents() {
		return components;
	}

	protected void addComponent(Component component) {
		Objects.requireNonNull(component);
		for (Component other : getComponents())
			if (component.equals(other) || other.getClass().equals(component.getClass()))
				throw new RuntimeException("Component of same type already registered");
		components.add(component);
	}

	public <T extends Component> boolean hasComponent(Class<T> cls) {
		Objects.requireNonNull(cls);
		for (Component component : getComponents())
			if (cls.equals(component.getClass()))
				return true;
		return false;
	}

	@SuppressWarnings("unchecked")
	public <T extends Component> T getComponent(Class<T> cls) {
		Objects.requireNonNull(cls);
		for (Component component : getComponents())
			if (cls.equals(component.getClass()))
				return (T) component;
		return null;
	}

}
