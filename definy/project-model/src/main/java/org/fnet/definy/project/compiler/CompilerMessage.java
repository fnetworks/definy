package org.fnet.definy.project.compiler;

import java.io.File;
import java.util.regex.Matcher;

public class CompilerMessage {

	private File file;
	private int line, column;
	private String severity;
	private String message;

	public CompilerMessage(File file, int line, int column, String severity, String message) {
		this.file = file;
		this.line = line;
		this.column = column;
		this.severity = severity;
		this.message = message;
	}

	/**
	 * Group names:
	 * <ul>
	 * <li>file</li>
	 * <li>line</li>
	 * <li>column</li>
	 * <li>severity</li>
	 * <li>message</li>
	 * </ul>
	 */
	public static CompilerMessage fromPattern(Matcher matcher) {
		return new CompilerMessage(
				new File(matcher.group("file")),
				Integer.parseInt(matcher.group("line")),
				Integer.parseInt(matcher.group("column")),
				matcher.group("severity"),
				matcher.group("message")
		);
	}

	public File getFile() {
		return file;
	}

	public int getLine() {
		return line;
	}

	public int getColumn() {
		return column;
	}

	public String getSeverity() {
		return severity;
	}

	public String getMessage() {
		return message;
	}
}
