package org.fnet.definy.project.compiler;

import org.fnet.definy.project.Module;
import org.fnet.definy.util.streamprinter.PrintTarget;

import java.io.File;
import java.util.List;

public interface ICompiler {

	void compile(File[] sources, File output, Module module, PrintTarget printTarget,
	             List<CompilerMessage> compilerMessages) throws Exception;

}
