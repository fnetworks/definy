package org.fnet.definy.util.streamprinter;

@FunctionalInterface
public interface PrintTarget {
	void print(String string, TextType type);
}
