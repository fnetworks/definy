package org.fnet.definy.util;

public class FormatUtil {

	/**
	 * Converts nanoseconds to "human readable" time
	 *
	 * @param ns The time in nanoseconds
	 * @return The formatted time
	 */
	public static String nsToHumanReadable(double ns) {
		if (ns < 1000) {
			return ns + " ns";
		} else if (ns / 1000 < 1000) {
			return (double) Math.round(ns / 100) / 10 + " µs";
		} else if (ns / 1000000 < 1000) {
			return (double) Math.round(ns / 100000) / 10 + " ms";
		} else {
			return (double) Math.round(ns / 100000000) / 10 + " s";
		}
	}

	/**
	 * Converts milliseconds to "human readable" time
	 *
	 * @param ms The time in milliseconds
	 * @return The formatted time
	 */
	public static String msToHumanReadable(double ms) {
		if (ms < 1000) {
			return ms + " ms";
		} else {
			return (double) Math.round(ms / 100) / 10 + " s";
		}
	}

}
