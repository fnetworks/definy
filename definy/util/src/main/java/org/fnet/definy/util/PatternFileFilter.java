package org.fnet.definy.util;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;

public class PatternFileFilter implements FileFilter {

	private Pattern pattern;

	public PatternFileFilter(Pattern pattern) {
		this.pattern = pattern;
	}

	@Override
	public boolean accept(File pathname) {
		return pattern.matcher(pathname.getName()).matches();
	}
}
