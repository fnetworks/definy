package org.fnet.definy.util;

/**
 * Wrapper around {@link Thread#getStackTrace()}
 */
public class CallStack {

	private CallStackElement[] callStack;

	/**
	 * Creates a new call stack starting at the caller method.
	 * <br/>
	 * <b>Potentially costly operation.</b>
	 */
	public CallStack() {
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		callStack = new CallStackElement[stackTrace.length - 1];
		for (int i = 1; i < stackTrace.length; i++)
			callStack[i - 1] = new CallStackElement(stackTrace[i]);
	}

	/**
	 * @return The amount of calls in this call stack.
	 */
	public int getCallCount() {
		return callStack.length;
	}

	/**
	 * Gets the call stack.
	 *
	 * @return The stack trace elements
	 */
	public CallStackElement[] getCallStack() {
		return callStack;
	}
}
