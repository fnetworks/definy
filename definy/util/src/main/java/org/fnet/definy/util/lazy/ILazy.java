package org.fnet.definy.util.lazy;

public interface ILazy<T> {

	T getValue();

	default T get() {
		return getValue();
	}

	boolean isInitialized();

}
