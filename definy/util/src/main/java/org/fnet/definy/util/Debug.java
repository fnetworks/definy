package org.fnet.definy.util;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Debug {

	public static final boolean ENABLED = Boolean.parseBoolean(System.getProperty("definy.debug", "false"));

	private static File runtimeReportDirectory;

	public static File getReportsDirectory() {
		return new File(CommonDirectories.getTempDirectory(), "reports");
	}

	/**
	 * @return The report directory for the current runtime
	 * @throws IllegalStateException If debugging is not enabled
	 */
	public static File getCurrentReportDirectory() {
		if (!ENABLED)
			throw new IllegalStateException("Debbuging is not active");
		if (runtimeReportDirectory == null)
			runtimeReportDirectory = new File(Debug.getReportsDirectory(),
					LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss")));
		return runtimeReportDirectory;
	}

}
