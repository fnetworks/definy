package org.fnet.definy.util;

import org.fnet.definy.util.streamprinter.DualStreamPrinter;
import org.fnet.definy.util.streamprinter.PrintTarget;
import org.fnet.definy.util.streamprinter.TextType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class ProcessUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessUtil.class);

	public static void runProcess(List<String> args) throws IOException, InterruptedException {
		LOGGER.debug(String.join(" ", args));
		ProcessBuilder builder = new ProcessBuilder(args);
		builder.inheritIO();
		int exitCode = builder.start().waitFor();
		if (exitCode != 0)
			throw new RuntimeException("Process exited with " + exitCode);
	}

	public static void runProcess(String... args) throws IOException, InterruptedException {
		LOGGER.debug(String.join(" ", args));
		ProcessBuilder builder = new ProcessBuilder(args);
		builder.inheritIO();
		int exitCode = builder.start().waitFor();
		if (exitCode != 0)
			throw new RuntimeException("Process exited with " + exitCode);
	}

	public static void runProcess(PrintTarget target, List<String> args) throws IOException, InterruptedException {
		if (target != null)
			target.print(String.join(" ", args), TextType.STDOUT);
		else
			LOGGER.debug(String.join(" ", args));
		ProcessBuilder builder = new ProcessBuilder(args);
		if (target == null)
			builder.inheritIO();
		Process start = builder.start();
		if (target != null) {
			DualStreamPrinter reader = new DualStreamPrinter(start, target);
			reader.waitFor();
		}
		int exitCode = start.waitFor();
		if (exitCode != 0)
			throw new RuntimeException("Process exited with " + exitCode);
	}

	public static int runProcessNoThrow(PrintTarget target, List<String> args) throws IOException, InterruptedException {
		if (target != null)
			target.print(String.join(" ", args), TextType.STDOUT);
		else
			LOGGER.debug(String.join(" ", args));
		ProcessBuilder builder = new ProcessBuilder(args);
		if (target == null)
			builder.inheritIO();
		Process start = builder.start();
		if (target != null) {
			DualStreamPrinter reader = new DualStreamPrinter(start, target);
			reader.waitFor();
		}
		return start.waitFor();
	}

	public static void runProcess(PrintTarget target, String... args) throws IOException, InterruptedException {
		if (target != null)
			target.print(String.join(" ", args), TextType.STDOUT);
		else
			LOGGER.debug(String.join(" ", args));
		ProcessBuilder builder = new ProcessBuilder(args);
		if (target == null)
			builder.inheritIO();
		Process start = builder.start();
		if (target != null) {
			DualStreamPrinter reader = new DualStreamPrinter(start, target);
			reader.waitFor();
		}
		int exitCode = start.waitFor();
		if (exitCode != 0)
			throw new RuntimeException("Process exited with " + exitCode);
	}
}
