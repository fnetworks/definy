package org.fnet.definy.util.streamprinter;

import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class DualStreamPrinter {

	private Thread[] executorThreads;

	public DualStreamPrinter(InputStream stdout, InputStream stderr) {
		this(stdout, stderr, new SystemIOPrintTarget());
	}

	public DualStreamPrinter(Process process, PrintTarget target) {
		this(process.getInputStream(), process.getErrorStream(), target);
	}

	public DualStreamPrinter(InputStream stdout, InputStream stderr, PrintTarget target) {
		executorThreads = new Thread[2];
		executorThreads[0] = new Thread(new InternalPrinter(stdout, TextType.STDOUT, target));
		executorThreads[1] = new Thread(new InternalPrinter(stderr, TextType.STDERR, target));
		for (Thread t : executorThreads)
			t.start();
	}

	public DualStreamPrinter(Process p) {
		this(p, new SystemIOPrintTarget());
	}

	public void waitFor() throws InterruptedException {
		for (Thread t : executorThreads)
			t.join();
	}

	private static class InternalPrinter implements Runnable {
		private InputStream stream;
		private TextType textType;
		private PrintTarget target;

		private InternalPrinter(InputStream stream, TextType textType, PrintTarget target) {
			this.stream = stream;
			this.textType = textType;
			this.target = target;
		}

		@Override
		public void run() {
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))) {
				String line;
				while ((line = reader.readLine()) != null)
					target.print(line, textType);
			} catch (IOException e) {
				LoggerFactory.getLogger(DualStreamPrinter.class).error("Error while reading input stream", e);
			}
		}
	}

}
