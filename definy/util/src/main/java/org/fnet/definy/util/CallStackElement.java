package org.fnet.definy.util;

import org.jetbrains.annotations.NotNull;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * Wrapper around {@link StackTraceElement}
 */
public class CallStackElement {

	@NotNull
	private StackTraceElement original;

	public CallStackElement(@NotNull StackTraceElement original) {
		Objects.requireNonNull(original, "The original StackTraceElement can't be null");
		this.original = original;
	}

	/**
	 * @return The original {@link StackTraceElement}
	 */
	@NotNull
	public StackTraceElement getOriginal() {
		return original;
	}

	/**
	 * Returns the name of the source file containing the execution point
	 * represented by this stack trace element.  Generally, this corresponds
	 * to the {@code SourceFile} attribute of the relevant {@code class}
	 * file (as per <i>The Java Virtual Machine Specification</i>, Section
	 * 4.7.7).  In some systems, the name may refer to some source code unit
	 * other than a file, such as an entry in source repository.
	 * <p>
	 * <br/>
	 * <i>Documentation copied from {@link StackTraceElement#getFileName()}</i>
	 *
	 * @return the name of the file containing the execution point
	 * represented by this stack trace element, or {@code null} if
	 * this information is unavailable.
	 */
	public String getFileName() {
		return original.getFileName();
	}

	/**
	 * Returns the line number of the source line containing the execution
	 * point represented by this stack trace element.  Generally, this is
	 * derived from the {@code LineNumberTable} attribute of the relevant
	 * {@code class} file (as per <i>The Java Virtual Machine
	 * Specification</i>, Section 4.7.8).
	 * <p>
	 * <br/>
	 * <i>Documentation copied from {@link StackTraceElement#getFileName()}</i>
	 *
	 * @return the line number of the source line containing the execution
	 * point represented by this stack trace element, or a negative
	 * number if this information is unavailable.
	 */
	public int getLineNumber() {
		return original.getLineNumber();
	}

	/**
	 * Returns the fully qualified name of the class containing the
	 * execution point represented by this stack trace element.
	 * <p>
	 * <br/>
	 * <i>Documentation copied from {@link StackTraceElement#getFileName()}</i>
	 *
	 * @return the fully qualified name of the {@code Class} containing
	 * the execution point represented by this stack trace element.
	 */
	public String getClassName() {
		return original.getClassName();
	}

	/**
	 * Gets the {@link Class} of the call. This is the resolved class name.
	 *
	 * @return The resolved call class or null if the class couldn't be resolved.
	 * @see CallStackElement#getClassName
	 */
	public Class<?> getElementClass() {
		try {
			return Class.forName(getClassName());
		} catch (ClassNotFoundException e) {
			LoggerFactory.getLogger(getClass()).warn("Error getting call stack element class", e);
			return null;
		}
	}

	/**
	 * Returns the name of the method containing the execution point
	 * represented by this stack trace element.  If the execution point is
	 * contained in an instance or class initializer, this method will return
	 * the appropriate <i>special method name</i>, {@code <init>} or
	 * {@code <clinit>}, as per Section 3.9 of <i>The Java Virtual
	 * Machine Specification</i>.
	 * <p>
	 * <br/>
	 * <i>Documentation copied from {@link StackTraceElement#getFileName()}</i>
	 *
	 * @return the name of the method containing the execution point
	 * represented by this stack trace element.
	 */
	public String getMethodName() {
		return original.getMethodName();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof CallStackElement)) return false;
		CallStackElement that = (CallStackElement) o;
		return original.equals(that.original);
	}

	@Override
	public int hashCode() {
		return Objects.hash(original);
	}
}
