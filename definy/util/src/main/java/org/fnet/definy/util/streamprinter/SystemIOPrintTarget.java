package org.fnet.definy.util.streamprinter;

public class SystemIOPrintTarget implements PrintTarget {
	@Override
	public void print(String string, TextType type) {
		switch (type) {
			case STDERR:
				System.err.println(string);
				break;
			case STDOUT:
				System.out.println(string);
				break;
		}
	}
}
