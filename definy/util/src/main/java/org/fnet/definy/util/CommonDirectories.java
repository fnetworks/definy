package org.fnet.definy.util;

import org.fnet.definy.util.lazy.ILazy;
import org.fnet.definy.util.lazy.Lazy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Collection of static, lazily initialized common directories.
 */
public class CommonDirectories {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommonDirectories.class);


	private static final ILazy<File> CURRENT_DIRECTORY = new Lazy<>(() -> new File(System.getProperty("user.dir")));

	private static final ILazy<URL> CODE_SOURCE = new Lazy<>(() -> {
		CallStack callStack = new CallStack();
		Class<?> mainClass = callStack.getCallStack()[callStack.getCallCount() - 1].getElementClass();
		if (mainClass == null) {
			LOGGER.warn("Main class is null, using current");
			mainClass = CommonDirectories.class;
		}
		return mainClass.getProtectionDomain().getCodeSource().getLocation();
	});

	private static final ILazy<File> CODE_SOURCE_FILE = new Lazy<>(() -> {
		URL codeSource = getCodeSource();
		if (!codeSource.getProtocol().equals("file")) {
			LOGGER.warn("Code source is not a file");
			return null;
		}
		try {
			return new File(codeSource.toURI());
		} catch (URISyntaxException e) {
			LOGGER.warn("Failed to convert code source to directory", e);
			return null;
		}
	});

	private static final ILazy<File> PROGRAM_DIRECTORY = new Lazy<>(() -> {
		String programDirectoryProperty = System.getProperty("program.dir");
		if (programDirectoryProperty != null)
			return new File(programDirectoryProperty);

		File codeSource = getCodeSourceLocation();
		if (codeSource == null) {
			LOGGER.warn("Failed to get code source, using current directory instead");
			return getCurrentDirectory();
		}

		if (codeSource.getName().endsWith(".jar"))
			codeSource = codeSource.getParentFile();
		if (codeSource.getName().matches("(libs?|classes)$"))
			codeSource = codeSource.getParentFile();
		return codeSource;
	});

	/**
	 * @return The {@link java.security.CodeSource}'s location {@link URL}
	 */
	public static URL getCodeSource() {
		return CODE_SOURCE.get();
	}

	/**
	 * @return The code source as {@link File}, or null if it can't be converted to a file
	 * @see CommonDirectories#getCodeSource()
	 */
	public static File getCodeSourceLocation() {
		return CODE_SOURCE_FILE.get();
	}

	/**
	 * The current (work) directory of the program
	 *
	 * @return The current directory
	 */
	public static File getCurrentDirectory() {
		return CURRENT_DIRECTORY.get();
	}

	/**
	 * Returns the program directory.
	 * If this is an IDE build, this returns the class directory (IntelliJ: "classes" gets stripped).
	 * If this is a gradle distribution build, it returns its root (the directory that contains "bin" and "lib")
	 * If this is a single jar, it returns the directory of the jar.
	 * If the jar/class root can't be recognized, or an error occured, the current directory is returned
	 *
	 * @return The program directory, or the current directory if an error occurs
	 */
	public static File getProgramDirectory() {
		return PROGRAM_DIRECTORY.get();
	}

	public static File getConfigDirectory() {
		return new File(getProgramDirectory(), "config");
	}

	public static File getTempDirectory() {
		return new File(getProgramDirectory(), "temp");
	}

}
