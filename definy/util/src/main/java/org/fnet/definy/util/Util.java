package org.fnet.definy.util;

import java.util.Enumeration;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Util {

	public static <T> Stream<T> enumerationAsStream(Enumeration<T> e) {
		return StreamSupport.stream(new MyAbstractSpliterator<>(e), false);
	}

	private static class MyAbstractSpliterator<T> extends Spliterators.AbstractSpliterator<T> {
		private final Enumeration<T> e;

		public MyAbstractSpliterator(Enumeration<T> e) {
			super(Long.MAX_VALUE, Spliterator.ORDERED);
			this.e = e;
		}

		public boolean tryAdvance(Consumer<? super T> action) {
			if (e.hasMoreElements()) {
				action.accept(e.nextElement());
				return true;
			}
			return false;
		}

		public void forEachRemaining(Consumer<? super T> action) {
			while (e.hasMoreElements()) action.accept(e.nextElement());
		}
	}
}
