package org.fnet.definy.util.lazy;

import java.util.function.Supplier;

public class ExternalLazy<T> implements ILazy<T> {

	private T value;
	private boolean initialized = false;

	public T initializeWith(T value) {
		this.value = value;
		this.initialized = true;
		return value;
	}

	public T getValue() {
		return value;
	}

	public T getOrInitialize(Supplier<T> valueSupplier) {
		if (initialized)
			return value;
		return initializeWith(valueSupplier.get());
	}

	public boolean isInitialized() {
		return initialized;
	}
}
