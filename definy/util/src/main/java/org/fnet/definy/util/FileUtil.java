package org.fnet.definy.util;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.regex.Pattern;

public class FileUtil {

	public static File[] listFiles(File f, FileFilter filter) throws IOException {
		return Files.walk(f.toPath())
				.map(Path::toFile)
				.filter(filter::accept)
				.toArray(File[]::new);
	}

	public static File[] listFiles(File f) throws IOException {
		return Files.walk(f.toPath())
				.map(Path::toFile)
				.toArray(File[]::new);
	}

	public static void mkdirs(File location) throws IOException {
		if (!location.exists() && !location.mkdirs())
			throw new IOException(String.format("Error while creating file %s", location));
	}

	public static String[] fileToStringArray(File[] value) {
		return Arrays.stream(value).map(File::toString).toArray(String[]::new);
	}

	public static File relativizeWithExt(File file, File base, File target, String ext) {
		return new File(target, relative(file, base) + ext);
	}

	public static File relativize(File file, File base, File target) {
		return new File(target, relative(file, base));
	}

	public static String relative(File file, File base) {
		return file.getAbsolutePath().replaceFirst("^" + Pattern.quote(base.getAbsolutePath()), "");
	}

	public static void delete(File file) throws IOException {
		Files.walkFileTree(file.toPath(), new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				if (exc != null)
					throw exc;
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	public static void rename(File from, File to) throws IOException {
		Files.move(from.toPath(), to.toPath());
	}

	public static void rename(File from, String newName) throws IOException {
		Files.move(from.toPath(), new File(from.getParentFile(), newName).toPath());
	}

}
