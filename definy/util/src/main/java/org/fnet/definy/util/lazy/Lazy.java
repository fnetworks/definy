package org.fnet.definy.util.lazy;

import java.util.function.Supplier;

public class Lazy<T> implements ILazy<T> {

	private T value;
	private Supplier<T> supplier;
	private boolean initialized = false;

	public Lazy(Supplier<T> supplier) {
		this.supplier = supplier;
	}

	public T getValue() {
		if (!initialized) {
			value = supplier.get();
			initialized = true;
		}
		return value;
	}

	public boolean isInitialized() {
		return initialized;
	}
}
