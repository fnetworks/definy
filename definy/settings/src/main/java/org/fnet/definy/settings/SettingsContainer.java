package org.fnet.definy.settings;

import org.fnet.definy.data.Persistable;

import java.lang.reflect.Field;

/**
 * Utility class for settings container to inherit from.
 */
public abstract class SettingsContainer implements Persistable {

	protected String namedField(String name) {
		Field field;
		try {
			field = getClass().getDeclaredField(name);
		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		}
		NamedSetting annotation = field.getAnnotation(NamedSetting.class);
		if (annotation == null || annotation.value().isEmpty())
			return name;
		return annotation.value();
	}

}
