package org.fnet.definy.settings;

import org.fnet.definy.data.DataObject;
import org.fnet.definy.util.CommonDirectories;

import java.io.File;

public class AppSettings extends SettingsContainer {

	private static final Settings<AppSettings> INSTANCE = new Settings<>(
			new File(CommonDirectories.getConfigDirectory(), "app.json"), new AppSettings());

	public static Settings<AppSettings> getAppSettings() {
		return INSTANCE;
	}

	private AppSettings() {
	}

	/**
	 * The location of the last opened project
	 */
	@NamedSetting
	private File lastProjectLocation;

	@Override
	public void load(DataObject data) {
		String lastProjectLocationName = namedField("lastProjectLocation");
		if (data.has(lastProjectLocationName))
			lastProjectLocation = new File(data.getString(lastProjectLocationName));
	}

	@Override
	public void save(DataObject data) {
		if (lastProjectLocation != null)
			data.setString(namedField("lastProjectLocation"), lastProjectLocation.getAbsolutePath());
	}

	/**
	 * @return The location of the last opened project.
	 */
	public File getLastProjectLocation() {
		return lastProjectLocation;
	}

	/**
	 * @param lastProjectLocation The location of the last opened project to set.
	 */
	public void setLastProjectLocation(File lastProjectLocation) {
		this.lastProjectLocation = lastProjectLocation;
	}

}
