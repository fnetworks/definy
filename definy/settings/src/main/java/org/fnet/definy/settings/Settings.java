package org.fnet.definy.settings;

import org.fnet.definy.data.DataObject;
import org.fnet.definy.data.DataSerializers;
import org.fnet.definy.data.Persistable;
import org.fnet.definy.util.FileUtil;

import java.io.File;
import java.io.IOException;

public class Settings<T extends Persistable> {

	private T settingsContainer;
	private File location;

	/**
	 * Creates a new settings instance with the specified container instance.
	 *
	 * @param location                  Where to store and load from
	 * @param settingsContainerInstance The instance of the settings container to use
	 */
	public Settings(File location, T settingsContainerInstance) {
		this.location = location;
		this.settingsContainer = settingsContainerInstance;
	}

	/**
	 * Creates a new settings instance with a newly created container.
	 *
	 * @param location          Where to store and load from
	 * @param settingsContainer The settings container
	 * @throws ReflectiveOperationException If the instantiation of the container class fails
	 */
	public Settings(File location, Class<T> settingsContainer) throws ReflectiveOperationException {
		this(location, settingsContainer.getConstructor().newInstance());
	}

	/**
	 * Saves the settings to the set location
	 *
	 * @throws IOException If an IO error occurs
	 */
	public void save() throws IOException {
		DataObject o = new DataObject();
		settingsContainer.save(o);
		FileUtil.mkdirs(location.getParentFile());
		DataSerializers.get(location).serialize(o, location);
	}

	/**
	 * Loads the settings from the set location
	 *
	 * @throws IOException If an IO error occurs
	 */
	public void load() throws IOException {
		if (!location.exists())
			return;
		DataObject o = DataSerializers.deserialize(location);
		settingsContainer.load(o);
	}

	/**
	 * @return The settings container (The class that stores the settings)
	 * @see Persistable
	 * @see SettingsContainer
	 */
	public T getContainer() {
		return settingsContainer;
	}

	/**
	 * @return The location where the settings are saved/loaded
	 */
	public File getLocation() {
		return location;
	}

	/**
	 * @param location The location where the settings should be saved/loaded
	 */
	public void setLocation(File location) {
		this.location = location;
	}
}
