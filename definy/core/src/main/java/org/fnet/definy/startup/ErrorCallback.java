package org.fnet.definy.startup;

@FunctionalInterface
public interface ErrorCallback<T extends Throwable> {

	void call(T err);

}
