package org.fnet.definy;

import com.alee.laf.WebLookAndFeel;
import org.fnet.definy.lang.Localizer;
import org.fnet.definy.plugin.PluginSystem;
import org.fnet.definy.settings.AppSettings;
import org.fnet.definy.startup.AppLifecycle;
import org.fnet.definy.ui.AlertUtility;
import org.fnet.definy.ui.MainFrame;
import org.fnet.definy.ui.SplashScreen;
import org.fnet.definy.ui.editor.EditorProviders;
import org.fnet.definy.util.CommonDirectories;
import org.jetbrains.annotations.NonNls;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Properties;

/**
 * Main application class to keep things like settings, command line arguments etc.
 */
public class Application {

	@NonNls
	private static final String ICON_LOCATION = "/icon.png";

	static Application instance;

	public static Application getApplication() {
		return instance;
	}

	private final Logger logger = LoggerFactory.getLogger(Application.class);
	private PluginSystem pluginSystem;
	private Properties buildProperties;
	private String[] arguments;
	private BufferedImage appIcon;
	private MainFrame mainFrame;
	private SplashScreen splashScreen;

	Application(String[] args, Properties buildProperties) {
		logger.info("Application startup");
		this.buildProperties = buildProperties;
		this.arguments = args;

		logger.debug("programDir = \"{}\", currentDir = \"{}\"", CommonDirectories.getProgramDirectory(), CommonDirectories.getCurrentDirectory());
		logger.debug("devBuild = {}, version = {}", !isDistribution(), getVersion());

		AppLifecycle lifecycle = new AppLifecycle();

//		lifecycle.addStartupTask("System Look and Feel",
//				() -> UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()))
//				.allowedToFail(true);

		lifecycle.addStartupTask("Splash Screen", (done, error) -> {
			splashScreen = new SplashScreen();
			SwingUtilities.invokeLater(() -> splashScreen.setVisible(true));
			splashScreen.addReadyEventListener(done::run);
		});

		lifecycle.addStartupTask("Icon", () -> {
			appIcon = ImageIO.read(Application.class.getResourceAsStream(ICON_LOCATION));
			if (appIcon != null)
				splashScreen.setIconImage(appIcon);
		}).allowedToFail(true);

		lifecycle.addStartupTask("Initialize Look and Feel", () -> {
			WebLookAndFeel.install();
			splashScreen.repaint();
		});

		lifecycle.addStartupTask("Load settings", () -> AppSettings.getAppSettings().load());

		lifecycle.addStartupTask("Load plugins", () -> {
			File pluginDirectory = new File(
					isDistribution()
							? CommonDirectories.getProgramDirectory()
							: CommonDirectories.getCurrentDirectory(),
					"plugins");
			pluginSystem = new PluginSystem(pluginDirectory);
			pluginSystem.loadAll();
		});

		lifecycle.addStartupTask("Register defaults", EditorProviders::registerDefault);

		lifecycle.addStartupTask("Main Frame", () -> {
			mainFrame = new MainFrame();
			if (appIcon != null)
				mainFrame.setIconImage(appIcon);

			mainFrame.addCloseListener(lifecycle::stop);

			splashScreen.setVisible(false);
			mainFrame.setVisible(true);
			splashScreen.dispose();
		});

		lifecycle.addStartupTask("Open last project", () -> {
			File lastProjectLocation = AppSettings.getAppSettings().getContainer().getLastProjectLocation();
			if (lastProjectLocation != null) {
				try {
					mainFrame.openProject(lastProjectLocation);
				} catch (Exception e) {
					AlertUtility.showAlert(mainFrame, e, Localizer.getString("mainframe.openproject.error"));
					logger.error("Error opening project", e);
				}
			}
		});

		lifecycle.addShutdownTask("Close splash screen", () -> {
			if (splashScreen != null)
				splashScreen.dispose();
		});

		lifecycle.addShutdownTask("Close main frame", () -> {
			if (mainFrame != null)
				mainFrame.dispose();
		});

		lifecycle.addShutdownTask("Shutdown plugin system", () -> pluginSystem.shutdown());

		lifecycle.addShutdownTask("Save settings", () -> AppSettings.getAppSettings().save());

		lifecycle.addStartupFinishedListener(() -> logger.info("Startup complete"));
		lifecycle.addShutdownFinishedListener(() -> logger.info("Shutdown complete"));
		lifecycle.addStartupErrorListener(err -> {
			AlertUtility.showAlert(mainFrame != null ? mainFrame : splashScreen, err, "Error while starting");
			lifecycle.stop();
		});

		lifecycle.start();
	}

	public BufferedImage getAppIcon() {
		return appIcon;
	}

	public SplashScreen getSplashScreen() {
		return splashScreen;
	}

	/**
	 * @return The main program frame. Null before the frame is initialized.
	 */
	public MainFrame getMainFrame() {
		return mainFrame;
	}

	/**
	 * Determinates if the current program is a distribution build (built with gradle) or a IDE build.
	 *
	 * @return If this is a distribution build
	 */
	public boolean isDistribution() {
		return Boolean.parseBoolean(buildProperties.getProperty("distrib", "false"));
	}

	public String getVersion() {
		if (isDistribution())
			return buildProperties.getProperty("version", "unknown");
		else
			return "dev";
	}

	/**
	 * @return The application command line arguments
	 */
	public String[] getArguments() {
		return arguments;
	}


}
