package org.fnet.definy.startup;

public interface AsyncTask {

	void executeAsync(Runnable done, ErrorCallback<Exception> error);

}
