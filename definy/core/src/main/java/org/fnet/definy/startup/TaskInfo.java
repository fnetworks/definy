package org.fnet.definy.startup;

public class TaskInfo {

	private String name;
	private SyncTask task;
	private boolean allowedToFail;

	private long deltaTime;
	private boolean completed;

	public TaskInfo(String name, SyncTask task, boolean allowedToFail) {
		this.name = name;
		this.task = task;
		this.allowedToFail = allowedToFail;
	}

	public String getName() {
		return name;
	}

	public TaskInfo name(String name) {
		this.name = name;
		return this;
	}

	public SyncTask getTask() {
		return task;
	}

	public TaskInfo task(SyncTask task) {
		this.task = task;
		return this;
	}

	public TaskInfo task(AsyncTask task) {
		return task(new AsyncTaskWrapper(task));
	}

	public boolean isAllowedToFail() {
		return allowedToFail;
	}

	public TaskInfo allowedToFail(boolean allowedToFail) {
		this.allowedToFail = allowedToFail;
		return this;
	}

	/**
	 * @return The time in nanoseconds it took to execute the task.
	 */
	public long getDeltaTime() {
		return deltaTime;
	}

	void setDeltaTime(long deltaTime) {
		this.deltaTime = deltaTime;
	}

	public boolean isCompleted() {
		return completed;
	}

	void setCompleted(boolean completed) {
		this.completed = completed;
	}
}
