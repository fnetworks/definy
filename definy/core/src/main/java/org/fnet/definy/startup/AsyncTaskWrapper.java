package org.fnet.definy.startup;

public class AsyncTaskWrapper implements SyncTask {

	private AsyncTask task;
	private final Object lock = new Object();
	private Exception exception;
	private boolean end = false;

	public AsyncTaskWrapper(AsyncTask task) {
		this.task = task;
	}

	@Override
	public void execute() throws Exception {
		exception = null;

		task.executeAsync(() -> {
			end = true;
			synchronized (lock) {
				lock.notify();
			}
		}, err -> {
			end = true;
			exception = err;
			synchronized (lock) {
				lock.notify();
			}
		});

		if (!end) {
			synchronized (lock) {
				lock.wait();
			}
		}

		if (exception != null)
			throw exception;
	}
}
