package org.fnet.definy.startup;

public interface SyncTask {

	void execute() throws Exception;

}
