package org.fnet.definy.startup;

public class TaskInfoBuilder {
	private String name;
	private SyncTask task;
	private boolean allowedToFail;

	public TaskInfoBuilder name(String name) {
		this.name = name;
		return this;
	}

	public TaskInfoBuilder task(SyncTask task) {
		this.task = task;
		return this;
	}

	public TaskInfoBuilder allowedToFail(boolean allowedToFail) {
		this.allowedToFail = allowedToFail;
		return this;
	}

	public TaskInfo create() {
		return new TaskInfo(name, task, allowedToFail);
	}
}