package org.fnet.definy.startup;

import org.fnet.definy.util.Debug;
import org.fnet.definy.util.FileUtil;
import org.fnet.definy.util.FormatUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AppLifecycle {

	public enum Phase {
		BEFORE_START, STARTING, STARTED, ERRORED, SHUTTING_DOWN, SHUTDOWN
	}

	private List<TaskInfo> startupTasks = new LinkedList<>(), shutdownTasks = new LinkedList<>();
	private List<ErrorCallback<Exception>> startupErrorListener = new ArrayList<>(), shutdownErrorListener = new ArrayList<>();
	private List<Runnable> startupFinishedListener = new ArrayList<>(), shutdownFinishedListener = new ArrayList<>();

	private Phase phase = Phase.BEFORE_START;
	private Logger logger = LoggerFactory.getLogger(AppLifecycle.class);

	/**
	 * Adds a new startup task. The task isn't allowed to fail by default.
	 *
	 * @param name The name of the task
	 * @param task The action to run
	 * @return The created {@link TaskInfo}
	 */
	public TaskInfo addStartupTask(String name, AsyncTask task) {
		return addStartupTask(name, new AsyncTaskWrapper(task));
	}

	/**
	 * Adds a new startup task. The task isn't allowed to fail by default.
	 *
	 * @param name The name of the task
	 * @param task The action to run
	 * @return The created {@link TaskInfo}
	 */
	public TaskInfo addStartupTask(String name, SyncTask task) {
		TaskInfo info = new TaskInfo(name, task, false);
		this.startupTasks.add(info);
		return info;
	}

	/**
	 * Adds a new shutdown task. The task can fail by default.
	 *
	 * @param name The name of the task
	 * @param task The action to run
	 * @return The created {@link TaskInfo}
	 */
	public TaskInfo addShutdownTask(String name, AsyncTask task) {
		return addShutdownTask(name, new AsyncTaskWrapper(task));
	}

	/**
	 * Adds a new shutdown task. The task can fail by default.
	 *
	 * @param name The name of the task
	 * @param task The action to run
	 * @return The created {@link TaskInfo}
	 */
	public TaskInfo addShutdownTask(String name, SyncTask task) {
		TaskInfo info = new TaskInfo(name, task, true);
		this.shutdownTasks.add(info);
		return info;
	}

	public void addStartupErrorListener(ErrorCallback<Exception> callback) {
		this.startupErrorListener.add(callback);
	}

	public void addShutdownErrorListener(ErrorCallback<Exception> callback) {
		this.shutdownErrorListener.add(callback);
	}

	public void addStartupFinishedListener(Runnable callback) {
		this.startupFinishedListener.add(callback);
	}

	public void addShutdownFinishedListener(Runnable callback) {
		this.shutdownFinishedListener.add(callback);
	}

	public void start() {
		if (phase != Phase.BEFORE_START) {
			logger.warn("Illegal state " + phase);
			return;
		}
		phase = Phase.STARTING;
		for (TaskInfo taskInfo : startupTasks) {
			SyncTask task = taskInfo.getTask();
			logger.info("Task \"{}\" starting ...", taskInfo.getName());
			long startTime = System.nanoTime();
			try {
				task.execute();
				long endTime = System.nanoTime();
				long deltaTime = endTime - startTime;
				taskInfo.setDeltaTime(deltaTime);
				taskInfo.setCompleted(true);
				logger.info("Task \"{}\" finished after {}", taskInfo.getName(), FormatUtil.nsToHumanReadable(deltaTime));
			} catch (Exception e) {
				if (taskInfo.isAllowedToFail()) {
					logger.warn("Task \"" + taskInfo.getName() + "\" errored", e);
				} else {
					logger.error("Task \"" + taskInfo.getName() + "\" errored", e);
					phase = Phase.ERRORED;
					startupErrorListener.forEach(handler -> handler.call(e));
					stop();
					return;
				}
			}
		}
		startupFinishedListener.forEach(Runnable::run);
		phase = Phase.STARTED;
	}

	public void stop() {
		if (phase != Phase.STARTED && phase != Phase.ERRORED) {
			logger.warn("Illegal state " + phase);
			return;
		}
		phase = Phase.SHUTTING_DOWN;
		for (TaskInfo taskInfo : shutdownTasks) {
			SyncTask task = taskInfo.getTask();
			logger.info("Task \"{}\" starting ...", taskInfo.getName());
			long startTime = System.nanoTime();
			try {
				task.execute();
				long endTime = System.nanoTime();
				long deltaTime = endTime - startTime;
				taskInfo.setDeltaTime(deltaTime);
				taskInfo.setCompleted(true);
				logger.info("Task \"{}\" finished after {}", taskInfo.getName(), FormatUtil.nsToHumanReadable(deltaTime));
			} catch (Exception e) {
				if (taskInfo.isAllowedToFail()) {
					logger.warn("Task \"{}\" errored: {}: {}", taskInfo.getName(), e.getClass().getSimpleName(), e.getMessage());
				} else {
					logger.error("Task \"{}\" errored: {}: {}", taskInfo.getName(), e.getClass().getSimpleName(), e.getMessage());
					shutdownErrorListener.forEach(handler -> handler.call(e));
					return;
				}
			}
		}
		shutdownFinishedListener.forEach(Runnable::run);
		phase = Phase.SHUTDOWN;
	}

	public void dumpDebugInfo() {
		try {
			File reportDirectory = Debug.getCurrentReportDirectory();
			FileUtil.mkdirs(reportDirectory);

			File startupDebugCSV = new File(reportDirectory, "startup_time.csv");
			try (PrintWriter writer = new PrintWriter(startupDebugCSV)) {
				for (TaskInfo task : startupTasks) {
					writer.printf("%s;%s%n", task.getName(), task.getDeltaTime());
				}
			}
			logger.debug("Wrote timing statistics to {}", startupDebugCSV);
		} catch (IOException e) {
			logger.error("Failed to write debug statistics", e);
		}
	}

	public Phase getPhase() {
		return phase;
	}
}
