package org.fnet.definy.lang;

import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.PropertyKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Central localization class.
 * Use {@link Localizer#getString(String)} to get a language string,
 * and {@link Localizer#instance()} to get the Localizer instance.
 */
public class Localizer {

	@NonNls
	private static final String RESOURCE_BUNDLE = "lang.definy";
	private static Localizer instance;

	/**
	 * @return The Localizer instance
	 */
	public static Localizer instance() {
		if (instance == null)
			instance = new Localizer();
		return instance;
	}

	private Locale locale = Locale.getDefault();
	private ResourceBundle bundle = ResourceBundle.getBundle(RESOURCE_BUNDLE, locale);
	private final Logger logger = LoggerFactory.getLogger(Localizer.class);

	/**
	 * This method gets a language string by a property key
	 *
	 * @param key The property key
	 * @return The language, or the key itself if there is no entry with the sepcified key
	 */
	public String get(@PropertyKey(resourceBundle = RESOURCE_BUNDLE) String key) {
		try {
			return bundle.getString(key);
		} catch (MissingResourceException e) {
			logger.warn("I18n key not found", e);
			return key;
		}
	}

	/**
	 * @return The current locale of the Localizer
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Static method to get a language string of the Localizer instance
	 *
	 * @param key The property key
	 * @return The language, or the key itself if there is no entry with the sepcified key
	 */
	public static String getString(@PropertyKey(resourceBundle = RESOURCE_BUNDLE) String key) {
		return instance().get(key);
	}

	public static String getString(@PropertyKey(resourceBundle = RESOURCE_BUNDLE) String key, Object... fmt) {
		return MessageFormat.format(getString(key), fmt);
	}

}
