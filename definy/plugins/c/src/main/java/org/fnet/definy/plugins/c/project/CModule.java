package org.fnet.definy.plugins.c.project;

import org.fnet.definy.data.DataObject;
import org.fnet.definy.project.Module;
import org.fnet.definy.project.Property;
import org.fnet.definy.project.SourceFile;
import org.fnet.definy.project.compiler.CompilerMessage;
import org.fnet.definy.project.compiler.ICompiler;
import org.fnet.definy.project.compiler.ILinter;
import org.fnet.definy.project.components.CompileComponent;
import org.fnet.definy.project.components.LintComponent;
import org.fnet.definy.project.components.SourceFileComponent;
import org.fnet.definy.util.FileUtil;
import org.fnet.definy.util.PatternFileFilter;
import org.fnet.definy.util.ProcessUtil;
import org.fnet.definy.util.streamprinter.PrintTarget;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class CModule extends Module {

	public static final Pattern
			C_FILE_PATTERN = Pattern.compile(".*\\.c"),
			H_FILE_PATTERN = Pattern.compile(".*\\.h"),
			C_H_FILE_PATTERN = Pattern.compile(".*\\.[ch]"),
			CPP_FILE_PATTERN = Pattern.compile(".*\\.c(c|pp|xx|\\+\\+)"),
			HPP_FILE_PATTERN = Pattern.compile(".*\\.h(h|pp|xx\\+\\+)"),
			CPP_HPP_FILE_PATTERN = Pattern.compile(".*\\.(c(c|pp|xx\\+\\+)|h(h|pp|xx|\\+\\+))");

	@Property
	private CLanguageKind kind;
	@Property
	private ModuleKind moduleKind;
	@Property
	private String[] libraries;
	@Property
	private File[] libraryLocations, includeLocations;
	@Property
	private String[] compilerFlags, linkerFlags;

	private ICompiler compiler;
	private ILinter linter;

	public CModule(File location) {
		super(location);
		GCC gcc = GCC.getInstance();
		this.compiler = gcc;
		this.linter = gcc;
	}

	@Override
	public void createComponents() {
		super.createComponents();

		Pattern pattern, headerPattern, codePattern;
		switch (kind) {
			case C:
				pattern = C_H_FILE_PATTERN;
				headerPattern = H_FILE_PATTERN;
				codePattern = C_FILE_PATTERN;
				break;
			case CPP:
				pattern = CPP_HPP_FILE_PATTERN;
				headerPattern = HPP_FILE_PATTERN;
				codePattern = CPP_HPP_FILE_PATTERN;
				break;
			default:
				throw new RuntimeException("Invalid kind: " + kind);
		}

		addComponent(new SourceFileComponent(this,
				new PatternFileFilter(pattern),
				sourceFile -> {
					sourceFile.getMetadata().setBoolean("isHeader", headerPattern.matcher(sourceFile.getFile().getName()).matches());
				},
				getSourceDirectory(),
				getIncludeDirectory()));

		addComponent(new CompileComponent(this, compiler));
		addComponent(new LintComponent(this, linter));
	}

	@Override
	public void build(PrintTarget outputTarget) throws Exception {
		List<File> inputFiles = new ArrayList<>();
		for (SourceFile sourceFile : getComponent(SourceFileComponent.class).getSourceFiles()) {
			if (!sourceFile.getMetadata().getBoolean("isHeader"))
				inputFiles.add(sourceFile.getFile());
		}
		File finalOutput = new File(getBinariesDirectory(), getNormalizedName());
		List<CompilerMessage> messages = new ArrayList<>();
		compiler.compile(inputFiles.toArray(new File[0]), finalOutput, this, outputTarget, messages);
		for (CompilerMessage message : messages) {
			System.out.println(message.getSeverity() + ": " + message.getMessage());
		}
	}

	@Override
	public void run(PrintTarget outputTarget) throws Exception {
		build(outputTarget);
		if (this.moduleKind == ModuleKind.EXECUTABLE) {
			File executable = new File(getBinariesDirectory(), getNormalizedName());
			ProcessUtil.runProcess(outputTarget, executable.getAbsolutePath());
		}
	}

	@Override
	public void load(DataObject data) {
		super.load(data);
		this.kind = CLanguageKind.forName(data.getString("language", "C"));
		this.moduleKind = ModuleKind.forName(data.getString("kind", "static"));
		if (data.has("includeLocations"))
			this.includeLocations = Arrays.stream(data.getStringArray("includeLocations")).map(File::new).toArray(File[]::new);
		if (data.has("libraryLocations"))
			this.libraryLocations = Arrays.stream(data.getStringArray("libraryLocations")).map(File::new).toArray(File[]::new);
		if (data.has("libraries"))
			this.libraries = data.getStringArray("libraries");
		if (data.has("compilerFlags"))
			this.compilerFlags = data.getStringArray("compilerFlags");
		if (data.has("linkerFlags"))
			this.linkerFlags = data.getStringArray("linkerFlags");
	}

	@Override
	public void save(DataObject data) {
		super.save(data);
		data.setString("type", "c");
		if (kind != null)
			data.setString("language", kind.getDefaultAlias());
		if (moduleKind != null)
			data.setString("kind", moduleKind.getDefaultAlias());
		if (includeLocations != null)
			data.setStringArray("includeLocations", FileUtil.fileToStringArray(includeLocations));
		if (libraryLocations != null)
			data.setStringArray("libraryLocations", FileUtil.fileToStringArray(libraryLocations));
		if (libraries != null)
			data.setStringArray("libraries", this.libraries);
		if (compilerFlags != null)
			data.setStringArray("compilerFlags", this.compilerFlags);
		if (linkerFlags != null)
			data.setStringArray("linkerFlags", this.linkerFlags);
	}

	public CLanguageKind getKind() {
		return kind;
	}

	public void setKind(CLanguageKind kind) {
		this.kind = kind;
	}

	public ModuleKind getModuleKind() {
		return moduleKind;
	}

	public void setModuleKind(ModuleKind moduleKind) {
		this.moduleKind = moduleKind;
	}

	public String[] getLibraries() {
		return libraries;
	}

	public void setLibraries(String[] libraries) {
		this.libraries = libraries;
	}

	public File[] getLibraryLocations() {
		return libraryLocations;
	}

	public void setLibraryLocations(File[] libraryLocations) {
		this.libraryLocations = libraryLocations;
	}

	public File[] getIncludeLocations() {
		return includeLocations;
	}

	public void setIncludeLocations(File[] includeLocations) {
		this.includeLocations = includeLocations;
	}

	public String[] getCompilerFlags() {
		return compilerFlags;
	}

	public void setCompilerFlags(String[] compilerFlags) {
		this.compilerFlags = compilerFlags;
	}

	public String[] getLinkerFlags() {
		return linkerFlags;
	}

	public void setLinkerFlags(String[] linkerFlags) {
		this.linkerFlags = linkerFlags;
	}

}
