package org.fnet.definy.plugins.c;

import org.fnet.definy.plugin.IPlugin;
import org.fnet.definy.plugins.c.project.CModuleProvider;
import org.fnet.definy.plugins.c.project.GCC;
import org.fnet.definy.project.ModuleFactory;
import org.fnet.definy.ui.editor.EditorRegistry;
import org.fnet.definy.ui.editor.LanguageContext;

import java.io.IOException;
import java.io.InputStream;

public class CPlugin implements IPlugin {

	@Override
	public void onLoad() throws IOException {
		LanguageContext ctx;
		try (InputStream cpp = getClass().getResourceAsStream("/cpp.json")) {
			ctx = new LanguageContext(cpp);
		}
		ctx.setLinter(GCC.getInstance());
		EditorRegistry.getInstance().registerEditorProvider(ctx);
		ModuleFactory.getInstance().addProvider(new CModuleProvider());
	}

	@Override
	public void onUnload() {
	}

}
