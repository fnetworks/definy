package org.fnet.definy.plugins.c.project;

import org.fnet.definy.project.Module;
import org.fnet.definy.project.ModuleProvider;
import org.fnet.definy.project.Project;

import java.io.File;

public class CModuleProvider implements ModuleProvider {
	@Override
	public String getName() {
		return "c";
	}

	@Override
	public Module provideModule(Project project, File location) {
		return new CModule(location);
	}
}
