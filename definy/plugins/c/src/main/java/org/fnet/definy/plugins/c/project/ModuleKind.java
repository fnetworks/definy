package org.fnet.definy.plugins.c.project;

import org.jetbrains.annotations.NonNls;

public enum ModuleKind {

	SHARED_LIB("shared"), STATIC_LIB("static"), EXECUTABLE("executable", "exec");

	private final String[] aliases;

	ModuleKind(@NonNls String... aliases) {
		if (aliases.length == 0)
			throw new RuntimeException("Need more than zero aliases");
		this.aliases = aliases;
	}

	public String[] getAliases() {
		return aliases;
	}

	public String getDefaultAlias() {
		return aliases[0];
	}

	public static ModuleKind forName(String alias) {
		for (ModuleKind kind : values())
			for (String a : kind.getAliases())
				if (a.equalsIgnoreCase(alias))
					return kind;
		return null;
	}

}
