package org.fnet.definy.plugins.c.project;

import org.jetbrains.annotations.NonNls;

public enum CLanguageKind {

	C("C"), CPP("C++", "CPP", "CXX");

	private String[] aliases;

	CLanguageKind(@NonNls String... aliases) {
		if (aliases.length == 0)
			throw new RuntimeException("Alias required");
		this.aliases = aliases;
	}

	public String[] getAliases() {
		return aliases;
	}

	public String getDefaultAlias() {
		return aliases[0];
	}

	public static CLanguageKind forName(String name) {
		for (CLanguageKind kind : values())
			for (String alias : kind.getAliases())
				if (alias.equalsIgnoreCase(name))
					return kind;
		return null;
	}
}
