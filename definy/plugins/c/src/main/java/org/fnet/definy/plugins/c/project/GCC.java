package org.fnet.definy.plugins.c.project;

import org.fnet.definy.project.Module;
import org.fnet.definy.project.compiler.CompilerMessage;
import org.fnet.definy.project.compiler.ILinter;
import org.fnet.definy.project.compiler.TwoStepCompiler;
import org.fnet.definy.util.ProcessUtil;
import org.fnet.definy.util.streamprinter.PrintTarget;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GCC extends TwoStepCompiler implements ILinter {

	private static final GCC INSTANCE = new GCC();

	public static GCC getInstance() {
		return INSTANCE;
	}

	private static final Pattern MESSAGE_PATTERN = Pattern.compile(
			"(?<file>([a-zA-Z]:)?([^:]+)):(?<line>\\d+):(?<column>\\d+):\\s(?<severity>\\w+\\s*\\w*):\\s(?<message>.+)");

	private GCC() {
	}

	@Override
	public void lint(File input, Module _module, List<CompilerMessage> compilerMessages) throws IOException, InterruptedException {
		CModule module = (CModule) _module;
		List<String> args = new ArrayList<>();
		args.add(module.getKind() == CLanguageKind.CPP ? "g++" : "gcc");
		args.add("-c");
		args.add(input.getAbsolutePath());
		args.add("-fsyntax-only");
		args.add("-fdiagnostics-parseable-fixits");
		args.add("-fmessage-length=0");
		args.add("-fno-diagnostics-show-caret");
		if (module.getIncludeLocations() != null)
			Arrays.stream(module.getIncludeLocations()).map(File::getAbsolutePath).map(e -> String.format("-I%s", e)).forEach(args::add);
		if (module.getCompilerFlags() != null)
			args.addAll(Arrays.asList(module.getCompilerFlags()));
		ProcessUtil.runProcessNoThrow((string, type) -> {
			Matcher matcher = MESSAGE_PATTERN.matcher(string);
			if (matcher.matches()) {
				CompilerMessage message = CompilerMessage.fromPattern(matcher);
				compilerMessages.add(message);
			}
		}, args);
	}

	@Override
	protected void compile(File source, File output, Module _module, PrintTarget printTarget, List<CompilerMessage> compilerMessages) throws Exception {
		CModule module = (CModule) _module;
		List<String> args = new ArrayList<>();
		args.add(module.getKind() == CLanguageKind.CPP ? "g++" : "gcc");
		args.add("-c");
		args.add(source.getAbsolutePath());
		args.add("-o");
		args.add(output.getAbsolutePath());
		args.add("-fdiagnostics-parseable-fixits");
		args.add("-fmessage-length=0");
		args.add("-fno-diagnostics-show-caret");
		if (module.getIncludeLocations() != null)
			Arrays.stream(module.getIncludeLocations()).map(File::getAbsolutePath).map(e -> String.format("-I%s", e)).forEach(args::add);
		if (module.getCompilerFlags() != null)
			args.addAll(Arrays.asList(module.getCompilerFlags()));
		ProcessUtil.runProcess((string, type) -> {
			Matcher matcher = MESSAGE_PATTERN.matcher(string);
			if (matcher.matches()) {
				CompilerMessage message = CompilerMessage.fromPattern(matcher);
				compilerMessages.add(message);
			} else {
				printTarget.print(string, type);
			}
		}, args);
	}

	@Override
	protected void link(File[] inputs, File output, Module _module, PrintTarget printTarget, List<CompilerMessage> compilerMessages) throws Exception {
		CModule module = (CModule) _module;
		List<String> args = new ArrayList<>();
		args.add(module.getKind() == CLanguageKind.CPP ? "g++" : "gcc");
		Arrays.stream(inputs).map(File::getAbsolutePath).forEach(args::add);
		args.add("-o");
		args.add(output.getAbsolutePath());
		args.add("-fdiagnostics-parseable-fixits");
		args.add("-fmessage-length=0");
		args.add("-fno-diagnostics-show-caret");
		if (module.getLibraries() != null)
			Arrays.stream(module.getLibraries()).map(e -> String.format("-l%s", e)).forEach(args::add);
		if (module.getLibraryLocations() != null)
			Arrays.stream(module.getLibraryLocations()).map(File::getAbsoluteFile).map(e -> String.format("-L%s", e)).forEach(args::add);
		if (module.getLinkerFlags() != null)
			args.addAll(Arrays.asList(module.getLinkerFlags()));
		ProcessUtil.runProcess((string, type) -> {
			Matcher matcher = MESSAGE_PATTERN.matcher(string);
			if (matcher.matches()) {
				CompilerMessage message = CompilerMessage.fromPattern(matcher);
				compilerMessages.add(message);
			} else {
				printTarget.print(string, type);
			}
		}, args);
	}
}
