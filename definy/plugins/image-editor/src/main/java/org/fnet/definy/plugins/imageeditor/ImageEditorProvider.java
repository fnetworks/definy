package org.fnet.definy.plugins.imageeditor;

import org.fnet.definy.ui.ImageView;
import org.fnet.definy.ui.editor.EditorProvider;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageEditorProvider implements EditorProvider {
	@Override
	public String getName() {
		return "Image Editor";
	}

	@Override
	public boolean canEdit(File file) {
		for (String suffix : ImageIO.getWriterFileSuffixes())
			if (file.getName().endsWith(suffix))
				return true;
		return false;
	}

	@Override
	public JComponent createEditor(File file) throws IOException {
		BufferedImage image = ImageIO.read(file);
		ImageView view = new ImageView(image);
		view.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
		return new JScrollPane(view);
	}
}
