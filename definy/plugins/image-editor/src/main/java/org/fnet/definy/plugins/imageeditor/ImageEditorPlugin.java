package org.fnet.definy.plugins.imageeditor;

import org.fnet.definy.plugin.IPlugin;
import org.fnet.definy.ui.editor.EditorRegistry;

public class ImageEditorPlugin implements IPlugin {
	@Override
	public void onLoad() {
		EditorRegistry.getInstance().registerEditorProvider(new ImageEditorProvider());
	}

	@Override
	public void onUnload() {
	}
}
