# Definy

An IDE focused on C/C++ development.

Definy aims to be very simple and stable, supporting mostly small, but also complex projects.

## Contributing

This project uses [Gradle](https://gradle.org/), so setting up your environment is pretty easy.

* IntelliJ: [See docs](https://www.jetbrains.com/help/idea/gradle.html#gradle_import)
* Eclipse: [See docs](https://marketplace.eclipse.org/content/buildship-gradle-integration#group-details)

## Running

If you're on Linux, replace ``gradlew`` with ``./gradlew`` in the following snippets.

### With IDE

Integrated plugins must be built and copied before running from an ide:

```
gradlew copyPlugins
```

IDEs like IntelliJ can normally automate this task.

**NOTE for IntelliJ:**  You might want to [turn on annotation processors](https://www.jetbrains.com/help/idea/compiler-annotation-processors.html#89d758dd)

### Without IDE

#### Building

```
gradlew build
```

#### Running

```
gradlew run
```